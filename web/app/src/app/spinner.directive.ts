import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appSpinner]'
})
export class SpinnerDirective {
  @HostBinding('class')
  elementClass = 'custom-theme';

  constructor() {}
}
