import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';

import { AuthenticateError, AuthenticationService } from '../../services/authentication.service';
import { AuthorizationService } from '../../services/authorization.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.sass']
})
export class SigninComponent implements OnInit {
  public signinForm: FormGroup;

  public isError = false;

  public errorMessage = '';

  public errorIcon: IconDefinition = faExclamationTriangle;

  private readonly authenticationService: AuthenticationService;

  private readonly authorizationService: AuthorizationService;

  private readonly router: Router

  private loginIsChange = false

  private passwordIsChange = false

  constructor(
    formBuilder: FormBuilder,
    authenticationService: AuthenticationService,
    authorizationService: AuthorizationService,
    router: Router,
  ) {
    this.signinForm = formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
      remember: ['']
    });
    this.authenticationService = authenticationService;
    this.authorizationService = authorizationService;
    this.router = router;
  }

  get loginIsValid(): boolean {
    if (!this.loginIsChange) {
      return false;
    }

    return this.signinForm.get('login')?.valid || false;
  }

  get loginIsInvalid(): boolean {
    if (!this.loginIsChange) {
      return false;
    }

    const control = this.signinForm.get('login');

    if (control) {
      return !control.valid;
    }

    return false
  }

  get passwordIsValid(): boolean {
    if (!this.passwordIsChange) {
      return false;
    }

    return this.signinForm.get('password')?.valid || false;
  }

  get passwordIsInvalid(): boolean {
    if (!this.passwordIsChange) {
      return false;
    }

    const control = this.signinForm.get('password');

    if (control) {
      return !control.valid;
    }

    return false
  }

  public ngOnInit(): void {
    this.signinForm
      .get('login')
      ?.valueChanges
      .subscribe(value => {
        if (value.length > 0) {
          this.loginIsChange = true;
        }
      });
    this.signinForm
      .get('password')
      ?.valueChanges
      .subscribe(value => {
        if (value.length > 0) {
          this.passwordIsChange = true;
        }
      });
  }

  public onSubmit(): void {
    this.loginIsChange = true;
    this.passwordIsChange = true;

    this.authenticationService
      .authenticate(
        this.signinForm.value.login,
        this.signinForm.value.password,
        this.signinForm.value.remember,
      )
      .subscribe({
        complete: async () => {
          await this.authorizationService.init();

          return this.router.navigateByUrl('dashboard');
        },
        error: (err: AuthenticateError) => {
          this.isError = true;
          this.errorMessage = err.message;
          this.errorIcon = err.icon;
        },
      })
  }
}
