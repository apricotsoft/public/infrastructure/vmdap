import { Component, OnInit } from '@angular/core';
import { UserHttpClient } from '../../clients/http/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass']
})
export class UserComponent implements OnInit {
  private readonly userHttpClient: UserHttpClient;

  constructor(
    userHttpClient: UserHttpClient,
  ) {
    this.userHttpClient = userHttpClient;
  }

  ngOnInit(): void {
    this.userHttpClient
      .getById()
      .subscribe({
        complete: () => console.log('ura'),
      });
  }
}
