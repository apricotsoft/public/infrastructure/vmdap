import { IconDefinition } from '@fortawesome/fontawesome-common-types';

import { Action } from './constants/action';

export interface Link {
  title: string;
  route: string,
  action: Action;
  icon: IconDefinition
}
