import { v4 } from 'uuid'

import { ErrorResponse } from '../../clients/interfaces';

export enum NotificationBackground {
  none = '',
  primary = 'bg-primary',
  secondary = 'bg-secondary',
  success = 'bg-success',
  danger = 'bg-danger',
  warning = 'bg-warning',
  info = 'bg-info',
  dark = 'bg-dark',
}

export enum Template {
  none = 0,
  httpError = 1,
}

export class Notification {
  private readonly id: string;

  private autoHide = true;

  private delay = 3000;

  private body = '';

  private header = '';

  private class?: string;

  private data?: unknown;

  private background = NotificationBackground.none;

  private bodyTemplate = Template.none;

  constructor() {
    this.id = v4();
  }

  public getId(): string {
    return this.id;
  }

  public getBody(): string {
    return this.body;
  }

  public setBody(body: string): Notification {
    this.body = body;

    return this;
  }

  public hasBody(): boolean {
    return this.body !== ''
  }

  public setBodyTemplate(template: Template): Notification {
    this.bodyTemplate = template;

    return this;
  }

  public getHeader(): string {
    return this.header;
  }

  public setHeader(header: string): Notification {
    this.header = header;

    return this;
  }

  public hasHeader(): boolean {
    return this.header !== '';
  }

  public getAutoHide(): boolean {
    return this.autoHide;
  }

  public setAutoHide(hide: boolean): Notification {
    this.autoHide = hide;

    return this;
  }

  public getDelay(): number {
    return this.delay;
  }

  public setDelay(delay: number): Notification {
    this.delay = delay;

    return this;
  }

  public getClass(): string | undefined {
    let background = '';

    switch (this.background) {
      case NotificationBackground.primary:
        background = 'bg-primary text-light';
        break;
      case NotificationBackground.danger:
        background = 'bg-danger text-light';
        break;
      case NotificationBackground.info:
        background = 'bg-info text-light';
        break;
      case NotificationBackground.success:
        background = 'bg-success text-light';
        break;
      case NotificationBackground.warning:
        background = 'bg-warning text-light';
        break;
      case NotificationBackground.secondary:
        background = 'bg-secondary text-light';
        break;
      case NotificationBackground.dark:
        background = 'bg-dark text-light';
        break;
    }

    return `${this.class} ${background} mb-2`;
  }

  public setClass(className: string): Notification {
    this.class = className;

    return this;
  }

  public getBackground(): NotificationBackground {
    return this.background;
  }

  public setBackground(background: NotificationBackground): Notification {
    this.background = background;

    return this;
  }

  public setData(data: unknown): Notification {
    this.data = data;

    return this;
  }

  public getDataErrorTemplate(): ErrorResponse {
    return this.data as ErrorResponse;
  }

  public isHttpErrorTemplate(): boolean {
    return this.bodyTemplate === Template.httpError;
  }
}
