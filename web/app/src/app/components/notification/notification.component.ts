import { Component, OnInit } from '@angular/core';

import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

import { NotificationService } from '../../services/notification.service';
import { Notification } from './notification';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.sass'],
})
export class NotificationComponent implements OnInit {
  private readonly notificationService: NotificationService;

  public readonly httpErrorIcon = faExclamationTriangle;

  constructor(
    notificationService: NotificationService,
  ) {
    this.notificationService = notificationService;
  }

  public ngOnInit(): void {}

  public get list(): Array<Notification> {
    return this.notificationService.getItems();
  }

  public hide(item: Notification): void {
    this.notificationService.remove(item);
  }
}
