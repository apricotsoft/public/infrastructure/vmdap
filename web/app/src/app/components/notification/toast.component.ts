import { Component, OnInit } from '@angular/core';

import { NgbToast } from '@ng-bootstrap/ng-bootstrap';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.sass']
})
export class ToastComponent extends NgbToast implements OnInit {
  public readonly closeIcon = faSignOutAlt;

  ngOnInit(): void {}
}
