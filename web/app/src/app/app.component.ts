import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  faChevronCircleLeft,
  faChevronCircleRight,
  faIdCardAlt,
  faSignOutAlt,
  faTachometerAlt,
  faUsers,
  faExclamationTriangle,
} from '@fortawesome/free-solid-svg-icons';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';

import { AuthenticationService } from './services/authentication.service';
import { Link } from './interfaces';
import { Action } from './constants/action';
import { AuthorizationService } from './services/authorization.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  public isCollapsed = false;

  public signOutIcon = faSignOutAlt;

  private readonly router: Router;

  private readonly authenticationService: AuthenticationService;

  private readonly authorizationService: AuthorizationService;

  private readonly links: Array<Link>;

  constructor(
    router: Router,
    authenticationService: AuthenticationService,
    authorizationService: AuthorizationService,
  ) {
    this.router = router;
    this.authenticationService = authenticationService;
    this.authorizationService = authorizationService;

    this.links = [
      {
        title: 'Dashboard',
        route: '/dashboard',
        action: Action.dashboard,
        icon: faTachometerAlt,
      },
      {
        title: 'Users',
        route: '/users',
        action: Action.userList,
        icon: faUsers,
      },
      {
        title: 'Roles',
        route: '/roles',
        action: Action.roleList,
        icon: faIdCardAlt,
      },
    ];
    // const n1 = new Notification()
    //   .setAutoHide(true)
    //   .setType(NotificationType.danger)
    //   .setDelay(1000000)
    //   .setBody('test')
    //   .setHeader('header');
    // const n2 = new Notification()
    //   .setAutoHide(true)
    //   .setType(NotificationType.success)
    //   .setDelay(1000000)
    //   .setBody('test')
    //   .setHeader('header');
    // this.notificationService.add(n1);
    // this.notificationService.add(n2);
  }

  public get arrowIcon(): IconDefinition {
    return this.isCollapsed ? faChevronCircleRight : faChevronCircleLeft;
  }

  public get isAuthenticated(): boolean {
    return this.authenticationService.isAuthenticated();
  }

  public getRouter(): Router {
    return this.router;
  }

  public getLinks(): Array<Link> {
    return this.links.filter((link) => {
      return this.authorizationService.canAction(link.action)
    });
  }

  public async ngOnInit(): Promise<void> {
    await this.authenticationService.onInit();

    if (
      this.authenticationService.isAuthenticated()
      && this.router.url !== '/dashboard'
    ) {
      await this.router.navigateByUrl('dashboard');
    }
  }

  public async signOut(): Promise<boolean> {
    this.authenticationService.logout();

    return this.router.navigateByUrl('signin');
  }
}
