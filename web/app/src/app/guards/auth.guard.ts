import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate, Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';

import { Observable } from 'rxjs';

import { AuthenticationService } from '../services/authentication.service';
import { AuthorizationService } from '../services/authorization.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  private readonly authenticationService: AuthenticationService;

  private readonly authorizationService: AuthorizationService;

  private readonly router: Router;

  constructor(
    authenticationService: AuthenticationService,
    authorizationService: AuthorizationService,
    router: Router,
  ) {
    this.authenticationService = authenticationService;
    this.authorizationService = authorizationService;
    this.router = router;
  }

  public async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Promise<boolean> {
    if (!this.authenticationService.isAuthenticated) {
      return this.router.navigateByUrl('signin');
    }

    await this.authenticationService
      .isRefreshTokens();

    await this.authorizationService
      .isSync();

    if (!this.authorizationService.canAction(route.data.action)) {
      if (state.url === '/dashboard') {
        this.authenticationService.logout();

        return this.router.navigateByUrl('signin');
      }

      return false;
    }

    return true;
  }
}
