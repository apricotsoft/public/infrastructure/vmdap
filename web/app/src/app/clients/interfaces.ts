export interface SigninRequest {
  login: string;
  password: string;
}

export interface SigninResponse {
  access: string;
  refresh: string;
}

export interface ExchangeRequest {
  token: string;
}

export interface ErrorResponse {
  code: number;
  details: string;
  message: string;
}

export interface SelfUserResponse {
  id: number;
  login: number;
  date?: Date;
  roles: Array<string>
}

export interface RoleRawResponse {
  id: number;
  name: string;
  description: string;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
  actions: Array<string>;
}

export interface RoleResponse {
  id: number;
  name: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt?: Date;
  actions: Array<string>;
}

export enum Order {
  none = 0,
  asc  = 1,
  desc = -1,
}

export enum RoleListSort {
  id = 'id',
  name = 'name',
  description = 'description',
  createdAt = 'created_at',
  updatedAt = 'updated_at',
  deletedAt = 'deleted_at',
}

export interface RoleListRawRequest {
  limit: number;
  skip: number,
  order: number,
  sort: string,
  names: Array<string>,
}

export interface RoleListRawResponse {
  result: Array<RoleRawResponse>;
  next?: RoleListRawRequest;
}

export interface RoleListRequest {
  limit: number;
  skip: number,
  order: Order,
  sort: RoleListSort,
  names: Array<string>,
}

export interface RoleListResponse {
  result: Array<RoleResponse>;
  next?: RoleListRequest;
}
