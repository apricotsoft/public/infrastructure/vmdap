import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, Subject, throwError } from 'rxjs';
import { catchError, switchMap, throttleTime } from 'rxjs/operators';

import { v4 } from 'uuid'

import {
  AuthenticationService,
  RefreshState,
} from '../services/authentication.service';
import {
  Notification,
  NotificationBackground,
  Template,
} from '../components/notification/notification';
import { NotificationService } from '../services/notification.service';

import { serverUrlPrefix } from './constants';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  private readonly authenticationService: AuthenticationService

  private readonly notificationService: NotificationService

  private readonly router: Router;

  private readonly exchangeUrl = `${serverUrlPrefix}/auth/exchange/`;

  private readonly logout = new Subject();

  constructor(
    authenticationService: AuthenticationService,
    notificationService: NotificationService,
    router: Router,
  ) {
    this.authenticationService = authenticationService;
    this.notificationService = notificationService;
    this.router = router;

    this.logout
      .pipe(throttleTime(250))
      .subscribe(async () => {
        this.authenticationService.logout()

        return this.router.navigateByUrl('signin')
      });
  }

  public intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next
      .handle(this.addedJwtHeader(request))
      .pipe(
        catchError((err) => this.errorHandler(
          request,
          next,
          err,
        ))
      );
  }

  private addedJwtHeader(request: HttpRequest<unknown>): HttpRequest<unknown> {
    const headers: Record<string, string> = {
      'X-Request-Id': v4(),
    };

    if (this.authenticationService.isAuthenticated()) {
      headers['Authorization'] = `Bearer ${this.authenticationService.token}`;
    }

    return request.clone({
      headers: new HttpHeaders(headers),
    });
  }

  private errorHandler(
    request: HttpRequest<unknown>,
    next: HttpHandler,
    err: any,
  ): Observable<HttpEvent<any>> {
    if (err instanceof HttpErrorResponse) {
      if (
        err.status === 401
        && err.error.message === 'token expired'
        && request.url !== this.exchangeUrl
      ) {
        return this
          .updateToken()
          .pipe(
            switchMap(() => {
              return next.handle(this.addedJwtHeader(request))
            })
          );
      }

      const notification = new Notification()
        .setAutoHide(true)
        .setBackground(NotificationBackground.danger)
        .setBodyTemplate(Template.httpError)
        .setData(err.error)
        .setDelay(5000)
      this.notificationService.add(notification);
    } else if (request.url === this.exchangeUrl) {
      return new Observable<never>((subscriber) => {
        this.logout.next();
        subscriber.error(err);
      })
    }

    return throwError(err);
  }

  private updateToken(): Observable<void> {
    this.authenticationService
      .getRefreshSubject()
      .next(RefreshState.inProgress);

    return new Observable<void>((subscriber) => {
      this.authenticationService
        .refreshToken()
        .subscribe({
          next: () => {
            this.authenticationService
              .getRefreshSubject()
              .next(RefreshState.done);
            subscriber.next();
          },
          error: (err: any) => {
            subscriber.error(err);
          }
        })
    });
  }
}
