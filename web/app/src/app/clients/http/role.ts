import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { serverUrlPrefix } from '../constants';
import { RoleListRawResponse, RoleListResponse, RoleRawResponse, RoleResponse } from '../interfaces';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoleHttpClient {
  private readonly http: HttpClient

  private readonly urlPrefix = `${serverUrlPrefix}/roles`

  constructor(http: HttpClient) {
    this.http = http;
  }

  public getByNames(names: Array<string>): Observable<Array<RoleResponse>> {
    return this.http
      .get<RoleListRawResponse>(
        `${this.urlPrefix}/`,
        {
          params: {
            names,
          }
        }
      )
      .pipe(
        map((res) => {
          return res.result.map((raw) => {
            const role: RoleResponse = {
              id: raw.id,
              name: raw.name,
              description: raw.description,
              actions: raw.actions,
              createdAt: new Date(raw.created_at),
              updatedAt: new Date(raw.updated_at),
            }

            if (raw.deleted_at) {
              role.deletedAt = new Date(raw.deleted_at);
            }

            return role;
          });
        })
      );
  }

  public get(): Observable<RoleListResponse> {
    return this.http
      .get<RoleListResponse>(
        `${this.urlPrefix}/`,
        {
          params: {
            limit: 1,
          }
        }
      )
      .pipe(
        map((response) => {
          response.result = response.result.map((role) => {
            role.createdAt = new Date(role.createdAt);
            role.updatedAt = new Date(role.updatedAt);

            if (role.deletedAt) {
              role.deletedAt = new Date(role.deletedAt);
            }

            return role;
          });

          return response;
        })
      );
  }
}
