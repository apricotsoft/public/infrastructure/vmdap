import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { serverUrlPrefix } from '../constants';
import { SelfUserResponse } from '../interfaces';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserHttpClient {
  private readonly http: HttpClient

  private readonly urlPrefix = `${serverUrlPrefix}/users`

  constructor(http: HttpClient) {
    this.http = http;
  }

  public self(): Observable<SelfUserResponse> {
    return this.http
      .get<SelfUserResponse>(
        `${this.urlPrefix}/self/`,
      )
      .pipe(
        map((user) => {
          if (user.date) {
            user.date = new Date(user.date);
          } else {
            delete user.date;
          }

          return user;
        })
      );
  }

  public getById(): Observable<SelfUserResponse> {
    return this.http
      .get<SelfUserResponse>(
        `${this.urlPrefix}/1/`,
      )
      .pipe(
        map((user) => {
          if (user.date) {
            user.date = new Date(user.date);
          } else {
            delete user.date;
          }

          return user;
        })
      );
  }
}
