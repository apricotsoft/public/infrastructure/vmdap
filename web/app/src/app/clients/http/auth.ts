import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { serverUrlPrefix } from '../constants';
import {
  ExchangeRequest,
  SigninRequest,
  SigninResponse,
} from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthHttpClient {
  private readonly http: HttpClient

  private readonly urlPrefix = `${serverUrlPrefix}/auth`

  constructor(http: HttpClient) {
    this.http = http;
  }

  public signin(req: SigninRequest): Observable<SigninResponse> {
    return this.http
      .post<SigninResponse>(
        `${this.urlPrefix}/`,
        req,
      );
  }

  public exchange(req: ExchangeRequest): Observable<SigninResponse> {
    return this.http
      .post<SigninResponse>(
        `${this.urlPrefix}/exchange/`,
        req,
      );
  }
}
