import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { serverUrlPrefix } from '../constants';
import { Observable } from 'rxjs';
import { SigninRequest, SigninResponse } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class DashboardHttpClient {
  private readonly http: HttpClient

  private readonly urlPrefix = `${serverUrlPrefix}/dashboard/`

  constructor(http: HttpClient) {
    this.http = http;
  }

  public info(): Observable<unknown> {
    return this.http.get(this.urlPrefix)
  }
}
