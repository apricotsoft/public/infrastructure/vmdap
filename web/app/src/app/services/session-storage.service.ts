import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {
  private readonly refreshTokenKey = 'refresh';
  private readonly tokenKey = 'access';

  public setRefreshToken(token: string): void {
    sessionStorage.setItem(this.refreshTokenKey, token);
  }

  public getRefreshToken(): string {
    return sessionStorage.getItem(this.refreshTokenKey) || '';
  }

  public deleteRefreshToken(): void {
    sessionStorage.removeItem(this.refreshTokenKey);
  }

  public setToken(token: string): void {
    sessionStorage.setItem(this.tokenKey, token);
  }

  public getToken(): string {
    return sessionStorage.getItem(this.tokenKey) || '';
  }

  public deleteToken(): void {
    sessionStorage.removeItem(this.tokenKey);
  }
}
