import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  private readonly refreshTokenKey = 'refresh';

  public setRefreshToken(token: string): void {
    localStorage.setItem(this.refreshTokenKey, token);
  }

  public getRefreshToken(): string {
    return localStorage.getItem(this.refreshTokenKey) || '';
  }

  public deleteRefreshToken(): void {
    localStorage.removeItem(this.refreshTokenKey);
  }
}
