import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import {
  faIdCard,
  faKey,
  faExclamationTriangle,
} from '@fortawesome/free-solid-svg-icons';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';

import { LocalStorageService } from './local-storage.service';
import { SessionStorageService } from './session-storage.service';
import { AuthHttpClient } from '../clients/http/auth';
import { ErrorResponse } from '../clients/interfaces';
import { AuthorizationService } from './authorization.service';
import { Router } from '@angular/router';

export interface AuthenticateError {
  message: string;
  icon: IconDefinition;
}

export enum RefreshState {
  inProgress = 'in progress',
  done = 'done',
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private readonly localStorage: LocalStorageService;

  private readonly sessionStorage: SessionStorageService;

  private readonly  authorizationService: AuthorizationService;

  private readonly authHttpClient: AuthHttpClient;

  private readonly refreshSubject = new BehaviorSubject<RefreshState>(RefreshState.done);

  private readonly router: Router;

  private authenticated = false;

  private accessToken = '';

  constructor(
    router: Router,
    ls: LocalStorageService,
    ss: SessionStorageService,
    authorizationService: AuthorizationService,
    ahc: AuthHttpClient,
  ) {
    this.localStorage = ls;
    this.sessionStorage = ss;
    this.authHttpClient = ahc;
    this.authorizationService = authorizationService;
    this.router = router;
  }

  public get token(): string {
    return this.accessToken;
  }

  public getRefreshSubject(): BehaviorSubject<RefreshState> {
    return this.refreshSubject;
  }

  public isAuthenticated(): boolean {
    return this.authenticated;
  }

  public logout(): void {
    this.sessionStorage.deleteToken();
    this.sessionStorage.deleteRefreshToken();
    this.localStorage.deleteRefreshToken();
    this.accessToken = '';
    this.authenticated = false;
  }

  public authenticate(
    login: string,
    password: string,
    remember: boolean,
  ): Observable<boolean> {
    return new Observable<boolean>((subscriber) => {
      this.authHttpClient
        .signin({
          login,
          password,
        })
        .subscribe({
          error: (err) => {
            const error = err.error as ErrorResponse;
            const message: AuthenticateError = {
              message: 'Oops',
              icon: faExclamationTriangle,
            };

            switch (error.code) {
              case 400:
                message.message = 'Incorrect password';
                message.icon = faKey;
                break;
              case 404:
                message.message = 'Incorrect login';
                message.icon = faIdCard;
                break;
            }

            subscriber.error(message);
          },
          next: (res) => {
            this.accessToken = res.access;
            this.sessionStorage.setToken(res.access);
            this.sessionStorage.deleteRefreshToken();
            this.localStorage.deleteRefreshToken();

            if (remember) {
              this.localStorage.setRefreshToken(res.refresh);
            } else {
              this.sessionStorage.setRefreshToken(res.refresh);
            }

            this.authenticated = true;
            subscriber.complete();
          }
        })
    });
  }

  public refreshToken(): Observable<boolean> {
    return new Observable<boolean>((subscriber) => {
      const token = this.sessionStorage.getRefreshToken()
        || this.localStorage.getRefreshToken();

      if (token === '') {
        subscriber.error(new Error('Refresh token not found'));
      }

      this.authHttpClient
        .exchange({ token })
        .subscribe({
          next: (res) => {
            this.accessToken = res.access;
            this.sessionStorage.setToken(res.access);

            if (this.localStorage.getRefreshToken()) {
              this.localStorage.setRefreshToken(res.refresh);
            } else {
              this.sessionStorage.setRefreshToken(res.refresh);
            }

            subscriber.next(true);
          }
        })
    });
  }

  public async onInit(): Promise<void> {
    const token = this.sessionStorage.getToken();

    if (token !== '') {
      this.authenticated = true;
      this.accessToken = token;

      await this.authorizationService.init();
    }
  }

  public async isRefreshTokens(): Promise<void> {
    return new Promise((resolve) => {
      this.refreshSubject
        .subscribe({
          next: (state) => {
            if (state === RefreshState.done) {
              resolve();
            }
          },
        });
    });
  }
}
