import { Injectable } from '@angular/core';

import { Notification } from '../components/notification/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private readonly items: Map<string, Notification>;

  constructor() {
    this.items = new Map<string, Notification>();
  }

  public add(item: Notification): void {
    this.items
      .set(
        item.getId(),
        item,
      );
  }

  public remove(item: Notification): void {
    this.items.delete(item.getId());
  }

  public getItems(): Array<Notification> {
    return Array.from<Notification>(this.items.values());
  }
}
