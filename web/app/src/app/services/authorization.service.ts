import { Injectable } from '@angular/core';

import { UserHttpClient } from '../clients/http/user';
import { RoleResponse, SelfUserResponse } from '../clients/interfaces';
import { RoleHttpClient } from '../clients/http/role';

import { RootRole } from '../constants/role';

import { SessionStorageService } from './session-storage.service';
import { Action } from '../constants/action';
import { filter, map, mergeMap, tap } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { RefreshState } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  private readonly sessionStorage: SessionStorageService;

  private readonly userHttpClient: UserHttpClient;

  private readonly roleHttpClient: RoleHttpClient;

  private readonly syncSubject = new BehaviorSubject<boolean>(false);

  private user?: SelfUserResponse;

  private isRoot = false;

  private actions: Array<string> = [];

  constructor(
    sessionStorage: SessionStorageService,
    userHttpClient: UserHttpClient,
    roleHttpClient: RoleHttpClient,
  ) {
    this.userHttpClient = userHttpClient;
    this.sessionStorage = sessionStorage;
    this.roleHttpClient = roleHttpClient
  }

  public async init(): Promise<void> {
    return new Promise((resolve) => {
      this.userHttpClient
        .self()
        .pipe(
          tap(() => {
            this.syncSubject.next(false);
          }),
          map<SelfUserResponse, boolean>((user) => {
            this.user = user;

            if (this.user.roles.includes(RootRole)) {
              this.isRoot = true;
            }

            return this.isRoot;
          }),
          mergeMap<boolean, Observable<Array<RoleResponse>>>((isRoot) => {
            if (this.user && !isRoot) {
              return this.roleHttpClient.getByNames(this.user.roles);
            }

            return new BehaviorSubject<Array<RoleResponse>>([])
          }),
        )
        .subscribe({
          next: (roles) => {
            this.actions = roles
              .map((role) => role.actions)
              .flat();
            this.syncSubject.next(true);

            resolve();
          },
        });
    });
  }

  public async isSync(): Promise<void> {
    return new Promise((resolve) => {
      this.syncSubject
        .subscribe({
          next: (state) => {
            if (state) {
              resolve();
            }
          },
        })
    });
  }

  public canAction(action: Action): boolean {
    return this.isRoot || this.actions.includes(action as string);
  }
}
