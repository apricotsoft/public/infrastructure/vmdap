export enum Action {
  dashboard = 'dashboard-info',
  userList = 'user-list',
  roleList = 'role-list',
}
