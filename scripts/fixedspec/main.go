package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"os"
)

type swagger struct {
	Consumes    json.RawMessage            `json:"consumes"`
	Produces    json.RawMessage            `json:"produces"`
	Schemes     json.RawMessage            `json:"schemes"`
	Swagger     json.RawMessage            `json:"swagger"`
	Info        json.RawMessage            `json:"info"`
	Paths       map[string]json.RawMessage `json:"paths"`
	Definitions json.RawMessage            `json:"definitions"`
}

func must(err error) {
	if err != nil {
		log.Panic(err)
	}
}

// Executed from Makefile to remove some endpoint prefix (e.g. "/admin-api")
// in swagger definition path (e.g. "./tmp/swagger.json").
func main() {
	sp := flag.String("path", "./tmp/swagger.json", "swagger definition path")
	flag.Parse()

	swaggerPath := *sp

	file, err := os.Open(swaggerPath)
	must(err)

	bytes, err := ioutil.ReadAll(file)
	must(err)

	var in swagger
	must(json.Unmarshal(bytes, &in))

	out := swagger{
		Consumes: in.Consumes,
		Produces: in.Produces,
		Schemes:  in.Schemes,
		Swagger:  in.Swagger,
		Info:     in.Info,
		//Paths:       make(map[string]json.RawMessage),
		Paths:       in.Paths,
		Definitions: in.Definitions,
	}

	//for path, spec := range in.Paths {
	//	out.Paths[fmt.Sprintf("/api%s", path)] = spec
	//}

	outSpec, err := json.Marshal(out)
	must(err)

	outFile, err := os.Create(swaggerPath)
	must(err)

	_, err = outFile.Write(outSpec)
	must(err)
}
