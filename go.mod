module gitlab.com/apricotsoft/public/infrastructure/vmdap

go 1.16

require (
	github.com/boltdb/bolt v1.3.1
	github.com/gbrlsnchs/jwt/v3 v3.0.1
	github.com/go-openapi/errors v0.20.0
	github.com/go-openapi/loads v0.20.0
	github.com/go-openapi/runtime v0.19.29
	github.com/go-openapi/spec v0.20.1
	github.com/go-openapi/strfmt v0.20.0
	github.com/go-openapi/swag v0.19.13
	github.com/go-openapi/validate v0.19.15
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/google/go-querystring v1.1.0
	github.com/jessevdk/go-flags v1.5.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mattn/go-colorable v0.1.8
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.25.0
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
