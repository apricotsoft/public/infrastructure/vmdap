package main

import (
	"errors"
	"flag"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/apps"
	"log"
)

func main() {
	configPath := flag.String(
		"c",
		"/etc/vmdap/config.yaml",
		"config file location",
	)

	flag.Parse()

	if configPath == nil {
		log.Fatal(errors.New("config path required"))
	}

	fmt.Println(
		fmt.Sprintf(
			"configuration file location: %s",
			*configPath,
		),
	)

	err := apps.Run(*configPath)

	if err != nil {
		log.Fatal(err)
	}
}
