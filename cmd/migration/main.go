package main

import (
	"errors"
	"flag"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
	"log"
)

func main() {
	name := flag.String(
		"n",
		"test",
		"migration name",
	)
	dbType := flag.String(
		"t",
		"sqlite",
		"database type",
	)

	flag.Parse()

	if name == nil || *name == "" {
		log.Fatal(errors.New("migration name required"))
	}

	if dbType == nil || *dbType == "" {
		log.Fatal(errors.New("database type required"))
	}

	gen, err := migrations.NewGenerator(*dbType, *name)

	if err != nil {
		log.Fatal(err)
	}

	err = gen.Generate()

	if err != nil {
		log.Fatal(err)
	}

	//path := "../../internal/pkg/database/sqlite/migrations"
}
