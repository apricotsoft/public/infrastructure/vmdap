package constants

type ServiceErrorMessage string

const (
	InternalServerError ServiceErrorMessage = "Internal Server Error"
	BadRequestError     ServiceErrorMessage = "Bad Request"
	NotFoundError       ServiceErrorMessage = "Not Found"
	UnauthorizedError   ServiceErrorMessage = "Unauthorized"
)
