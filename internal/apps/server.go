package apps

import (
	"context"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/config"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/bolt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/sqlite"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/security"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/repositories"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/services"
	httpServer "gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/server"
	"os"
	"os/signal"
	"syscall"
)

func Run(configPath string) error {
	cfg := config.NewConfig(configPath)
	err := cfg.Load()

	if err != nil {
		return fmt.Errorf("error creating config: %w", err)
	}

	log := logger.NewLogger(cfg.Logger)
	l := log.GetLogger()
	l.Info().Msg("Virtual machine dispatcher and packer starting")

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	boltClient, err := bolt.NewBolt(log, cfg.DB.Bolt)

	if err != nil {
		l.Error().Err(err).Send()

		return fmt.Errorf("error creating bolt client: %w", err)
	}

	sqliteClient, err := sqlite.NewSqlite(log, cfg)

	if err != nil {
		l.Error().Err(err).Send()

		return fmt.Errorf("error creating sqlite client: %w", err)
	}

	err = sqliteClient.Migrate(ctx, cfg.DB.SqlLite.Version)

	if err != nil {
		l.Error().Err(err).Send()

		return fmt.Errorf("error migrate sqlite: %w", err)
	}

	err = sqliteClient.Seed(ctx, cfg.DB.Seed.Version)

	if err != nil {
		l.Error().Err(err).Send()

		return fmt.Errorf("error sqlite seed: %w", err)
	}

	pass := security.NewPassword(cfg.App.Security.Cost)
	jwt := security.NewJwt(cfg.App.Security.Jwt)

	repos := repositories.NewRepositories(sqliteClient, boltClient, log)

	params := services.Params{
		Repositories: repos,
		Logger:       log,
		Config:       cfg.App,
		Security: services.SecurityParams{
			Password: pass,
			Jwt:      jwt,
		},
	}
	s := services.NewServices(&params)

	errCh := make(chan error)

	hs, err := httpServer.NewServer(s, log, cfg, jwt)

	if err != nil {
		l.Error().Err(err).Send()

		return fmt.Errorf("error creating http server: %w", err)
	}

	go func() {
		err = hs.Start(ctx)

		if err != nil {
			errCh <- err
		}
	}()

	go func() {
		err = boltClient.Sweep(ctx)

		if err != nil {
			errCh <- err
		}
	}()

	l.Info().Msg("Virtual machine dispatcher and packer started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)

	gr := make(chan struct{}, 1)

	for {
		select {
		case err = <-errCh:
			l.Error().Err(err).Send()
			l.Info().Msg("stopping gracefully...")
			gr <- struct{}{}
		case q := <-quit:
			l.Info().Msg(q.String())
			l.Info().Msg("signal received, stopping gracefully...")
			gr <- struct{}{}
		case <-gr:
			err = hs.Stop()

			if err != nil {
				l.Error().Err(err).Send()
			}

			l.Info().Msg("http server is closed")

			err = sqliteClient.Close()

			if err != nil {
				l.Error().Err(err).Send()
			}

			l.Info().Msg("sqlite is closed")

			return nil
		}
	}
}
