package services

import (
	"context"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/constants"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/repositories"
	"net/http"
	"time"
)

type GetUserByIdRequest struct {
	Meta
	Id     uint64
	IsSelf bool
}

type GetSelfUserRequest struct {
	Meta
	Id     uint64
	IsSelf bool
}

type UserResponse struct {
}

type SelfUserResponse struct {
	Id    uint64
	Login string
	Date  time.Time
	Roles []*domains.Role
}

type UserService struct {
	Repositories *repositories.Repositories
	Security     SecurityParams
	Logger       *logger.Log
}

func newUserService(p *Params) *UserService {
	return &UserService{
		Repositories: p.Repositories,
		Security:     p.Security,
		Logger:       p.Logger.GetLogger("user-services"),
	}
}

func (u UserService) GetById(ctx context.Context, params GetUserByIdRequest) (*UserResponse, error) {
	return nil, nil
}

func (u UserService) GetSelf(ctx context.Context, params GetSelfUserRequest) (*SelfUserResponse, error) {
	user, err := u.Repositories.Users.GetById(ctx, params.GetUserId())

	if err != nil {
		return nil, NewServiceError(
			constants.NotFoundError,
			http.StatusNotFound,
		).
			Err(err)
	}

	userRoles, err := u.Repositories.UserRoles.GetByUserId(ctx, params.GetUserId())

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	roleIds := make([]uint64, 0, len(userRoles))

	for _, item := range userRoles {
		roleIds = append(roleIds, item.RoleId)
	}

	roles, err := u.Repositories.Roles.GetByIds(ctx, roleIds)

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	err = u.Repositories.Activities.Set(domains.Activity{
		UserId:    params.GetUserId(),
		SessionId: params.GetSessionId(),
		Endpoint:  params.GetEndpoint(),
		Method:    params.GetMethod(),
	})

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	return &SelfUserResponse{
		Id:    user.Id,
		Login: user.Login,
		Date:  user.CreatedAt,
		Roles: roles,
	}, nil
}
