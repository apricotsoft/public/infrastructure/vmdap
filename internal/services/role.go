package services

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/constants"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/repositories"
	"net/http"
	"strings"
	"time"
)

type RoleListRequest struct {
	Meta
	Limit uint32
	Skip  uint32
	Order repositories.Order
	Sort  string
	Roles []string
}

type RoleResponse struct {
	Id          uint64
	Name        string
	Description string
	Actions     []string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}

type RoleListResponse struct {
	Result []*RoleResponse
	Next   *RoleListRequest
}

type RoleService struct {
	Repositories *repositories.Repositories
	Logger       *logger.Log
}

func newRoleService(p *Params) *RoleService {
	return &RoleService{
		Repositories: p.Repositories,
		Logger:       p.Logger.GetLogger("role-services"),
	}
}

func (r RoleService) List(ctx context.Context, params RoleListRequest) (*RoleListResponse, error) {
	var roles []*domains.Role
	var err error

	if len(params.Roles) > 0 {
		roles, err = r.Repositories.
			Roles.
			GetByNames(ctx, params.Roles)
	} else {
		err = validation.ValidateStruct(
			&params,
			validation.Field(&params.Limit, validation.Min(uint32(1))),
			validation.Field(&params.Skip, validation.Min(uint32(0))),
			validation.Field(
				&params.Order,
				validation.In(
					repositories.OrderAsc,
					repositories.OrderDesc,
					repositories.OrderNone,
				),
			),
			validation.Field(
				&params.Sort,
				validation.In(
					"id",
					"name",
					"description",
					"created_at",
					"updated_at",
					"deleted_at",
				),
			),
		)

		if err != nil {
			details := make([]string, 0, 4)

			for field, e := range err.(validation.Errors) {
				details = append(
					details,
					fmt.Sprintf(
						"field '%s' %s",
						field,
						e.Error(),
					),
				)
			}

			return nil, NewServiceError(
				constants.BadRequestError,
				http.StatusBadRequest,
			).
				Err(err).
				Details(strings.Join(details, ", "))
		}

		roles, err = r.Repositories.
			Roles.
			Get(
				ctx,
				params.Limit,
				params.Skip,
				params.Sort,
				params.Order,
			)
	}

	ids := make([]uint64, 0, len(roles))

	for _, item := range roles {
		ids = append(ids, item.Id)
	}

	roleActions, err := r.Repositories.RoleActions.GetByRoleIds(ctx, ids)

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	actionIds := make([]uint64, 0, len(roleActions))
	roleActionMap := make(map[uint64][]uint64)

	for _, item := range roleActions {
		if _, ok := roleActionMap[item.RoleId]; !ok {
			roleActionMap[item.RoleId] = make([]uint64, 0, 100)
		}

		roleActionMap[item.RoleId] = append(
			roleActionMap[item.RoleId],
			item.ActionId,
		)
		actionIds = append(actionIds, item.ActionId)

	}

	actions, err := r.Repositories.Actions.GetByIds(ctx, actionIds)

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	actionIdMap := make(map[uint64]*domains.Action)

	for _, action := range actions {
		actionIdMap[action.Id] = action
	}

	result := make([]*RoleResponse, 0, len(roles))

	for _, item := range roles {
		actionNames := make([]string, 0, 100)

		for _, actionId := range roleActionMap[item.Id] {
			actionNames = append(actionNames, actionIdMap[actionId].Name)
		}

		result = append(
			result,
			&RoleResponse{
				Id:          item.Id,
				Name:        item.Name,
				Actions:     actionNames,
				Description: item.Description,
				CreatedAt:   item.CreatedAt,
				UpdatedAt:   item.UpdatedAt,
				DeletedAt:   item.DeletedAt,
			},
		)
	}

	err = r.Repositories.Activities.Set(domains.Activity{
		UserId:    params.GetUserId(),
		SessionId: params.GetSessionId(),
		Endpoint:  params.GetEndpoint(),
		Method:    params.GetMethod(),
	})

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	res := &RoleListResponse{
		Result: result,
	}

	if len(result) >= int(params.Limit) {
		res.Next = &RoleListRequest{
			Limit: params.Limit,
			Skip:  params.Limit + params.Skip,
			Order: params.Order,
			Sort:  params.Sort,
			Roles: params.Roles,
		}
	}

	return res, nil
}
