package services

import (
	"context"
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/constants"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/security"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/repositories"
	"net/http"
	"strings"
	"time"
)

type SignInRequest struct {
	Meta
	Login    string
	Password string
}

type SignInResponse struct {
	Access  string
	Refresh string
}

type ExchangeRequest struct {
	Meta
	Token string
}

type AuthenticationService struct {
	Repositories *repositories.Repositories
	Security     SecurityParams
	Logger       *logger.Log
}

func newAuthenticationService(p *Params) *AuthenticationService {
	return &AuthenticationService{
		Repositories: p.Repositories,
		Security:     p.Security,
		Logger:       p.Logger.GetLogger("authentication-services"),
	}
}

func (a AuthenticationService) SignIn(ctx context.Context, params SignInRequest) (*SignInResponse, error) {
	start := time.Now()
	err := validation.ValidateStruct(
		&params,
		validation.Field(&params.Login, validation.Required),
		validation.Field(&params.Password, validation.Required),
	)

	if err != nil {
		details := make([]string, 0, 2)

		for field, e := range err.(validation.Errors) {
			details = append(
				details,
				fmt.Sprintf(
					"field '%s' %s",
					field,
					e.Error(),
				),
			)
		}

		return nil, NewServiceError(
			constants.BadRequestError,
			http.StatusBadRequest,
		).
			Err(err).
			Details(strings.Join(details, ", "))
	}

	ugt := time.Now()
	user, err := a.Repositories.Users.GetByLogin(ctx, params.Login)
	a.Logger.
		Debug().
		Dur("get user by login", time.Since(ugt)).
		Send()

	if err != nil {
		return nil, err
	}

	if user == nil {
		return nil, NewServiceError(
			constants.NotFoundError,
			http.StatusNotFound,
		).
			Details(
				fmt.Sprintf(
					"user with this '%s' login does not exist",
					params.Login,
				),
			)
	}

	pc := time.Now()
	_, err = a.Security.Password.Check(params.Password, user.Password)
	a.Logger.
		Debug().
		Dur("check password", time.Since(pc)).
		Send()

	if err != nil {
		return nil, NewServiceError(
			constants.BadRequestError,
			http.StatusBadRequest,
		).
			Err(err).
			Details("incorrect password")
	}

	sessionId := uuid.NewV4().String()

	je := time.Now()
	tokens, err := a.Security.Jwt.Encode(user.Id, sessionId)
	a.Logger.
		Debug().
		Dur("jwt encode", time.Since(je)).
		Send()

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	cs := time.Now()
	err = a.Repositories.Sessions.Create(
		ctx,
		sessionId,
		params.GetIp(),
		user.Id,
		tokens.Expired,
	)
	a.Logger.
		Debug().
		Dur("create session", time.Since(cs)).
		Send()

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	st := time.Now()
	err = a.Repositories.Activities.Set(domains.Activity{
		UserId:    user.Id,
		SessionId: sessionId,
		Endpoint:  params.GetEndpoint(),
		Method:    params.GetMethod(),
	})
	a.Logger.
		Debug().
		Dur("set activity", time.Since(st)).
		Send()

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	sto := time.Now()
	err = a.Repositories.Tokens.Set(tokens.Refresh, sessionId)
	a.Logger.
		Debug().
		Dur("set token", time.Since(sto)).
		Send()

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}
	a.Logger.
		Debug().
		Dur("finish", time.Since(start)).
		Send()

	return &SignInResponse{
		Access:  tokens.Access,
		Refresh: tokens.Refresh,
	}, nil
}

func (a AuthenticationService) Exchange(_ context.Context, params ExchangeRequest) (*SignInResponse, error) {
	err := validation.ValidateStruct(
		&params,
		validation.Field(&params.Token, validation.Required),
	)

	if err != nil {
		details := make([]string, 0, 2)

		for field, e := range err.(validation.Errors) {
			details = append(
				details,
				fmt.Sprintf(
					"field '%s' %s",
					field,
					e.Error(),
				),
			)
		}

		return nil, NewServiceError(
			constants.BadRequestError,
			http.StatusBadRequest,
		).
			Err(err).
			Details(strings.Join(details, ", "))
	}

	dbToken := a.Repositories.Tokens.Get(params.Token)

	if dbToken == "" {
		return nil, NewServiceError(
			constants.UnauthorizedError,
			http.StatusUnauthorized,
		).
			Details("invalid refresh token")
	}

	payload, err := a.Security.Jwt.Decode(params.Token)

	if err != nil {
		return nil, NewServiceError(
			constants.UnauthorizedError,
			http.StatusUnauthorized,
		).
			Err(err)
	}

	if payload.Type != security.TypeRefresh {
		return nil, NewServiceError(
			constants.UnauthorizedError,
			http.StatusUnauthorized,
		).
			Details("invalid token type")
	}

	tokens, err := a.Security.Jwt.Encode(payload.Id, payload.JWTID)

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	err = a.Repositories.Activities.Set(domains.Activity{
		UserId:    payload.Id,
		SessionId: payload.JWTID,
		Endpoint:  params.GetEndpoint(),
		Method:    params.GetMethod(),
	})

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	err = a.Repositories.Tokens.Set(tokens.Refresh, payload.JWTID)

	if err != nil {
		return nil, NewServiceError(
			constants.InternalServerError,
			http.StatusInternalServerError,
		).
			Err(err)
	}

	return &SignInResponse{
		Access:  tokens.Access,
		Refresh: tokens.Refresh,
	}, nil
}
