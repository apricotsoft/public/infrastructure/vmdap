package services

import (
	"context"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/constants"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/config"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/security"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/repositories"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/middlewares"
	"net/http"
)

type ServiceError struct {
	code    int32
	message constants.ServiceErrorMessage
	details string
	err     error
}

func NewServiceError(message constants.ServiceErrorMessage, code int32) *ServiceError {
	return &ServiceError{
		message: message,
		code:    code,
	}
}

func (se *ServiceError) Err(err error) *ServiceError {
	se.err = err

	return se
}

func (se *ServiceError) Details(details string) *ServiceError {
	se.details = details

	return se
}

func (se ServiceError) GetCode() int32 {
	return se.code
}

func (se ServiceError) GetDetail() string {
	return se.details
}

func (se ServiceError) GetMessage() string {
	return string(se.message)
}

func (se ServiceError) Error() string {
	detailsMsg := ""
	errMsg := ""

	if se.details != "" {
		detailsMsg = fmt.Sprintf(" details => %s,", se.details)
	}

	if se.err != nil {
		errMsg = fmt.Sprintf(" err => %s", se.err.Error())
	}

	return fmt.Sprintf(
		"service error: message => %s code => %d,%s%s",
		se.message,
		se.code,
		detailsMsg,
		errMsg,
	)
}

type Meta struct {
	endpoint  string
	method    domains.ActivityMethod
	userId    uint64
	sessionId string
	ip        string
}

func (rm *Meta) Load(req *http.Request) *Meta {
	rm.endpoint = req.RequestURI
	rm.ip = req.RemoteAddr

	switch req.Method {
	case "GET":
		rm.method = domains.ActivityMethodGet
	case "POST":
		rm.method = domains.ActivityMethodPost
	case "PUT":
		rm.method = domains.ActivityMethodPut
	case "DELETE":
		rm.method = domains.ActivityMethodDelete
	}

	if payload, ok := req.Context().Value(middlewares.CtxUserKey).(security.Payload); ok {
		rm.userId = payload.Id
		rm.sessionId = payload.JWTID
	}

	return rm
}

func (rm Meta) GetMethod() domains.ActivityMethod {
	return rm.method
}

func (rm Meta) GetEndpoint() string {
	return rm.endpoint
}

func (rm Meta) GetUserId() uint64 {
	return rm.userId
}

func (rm Meta) GetSessionId() string {
	return rm.sessionId
}

func (rm Meta) GetIp() string {
	return rm.ip
}

type VirtualMachines interface{}

type Networks interface{}

type Images interface{}

type Users interface {
	GetById(ctx context.Context, params GetUserByIdRequest) (*UserResponse, error)
	GetSelf(ctx context.Context, params GetSelfUserRequest) (*SelfUserResponse, error)
}

type Roles interface {
	List(ctx context.Context, params RoleListRequest) (*RoleListResponse, error)
}

type Authentication interface {
	SignIn(ctx context.Context, params SignInRequest) (*SignInResponse, error)
	Exchange(ctx context.Context, params ExchangeRequest) (*SignInResponse, error)
}

type Ansible interface{}

type Services struct {
	VirtualMachines VirtualMachines
	Networks        Networks
	Images          Images
	Users           Users
	Roles           Roles
	Authentication  Authentication
	Ansible         Ansible
}

type SecurityParams struct {
	Password *security.Password
	Jwt      *security.Jwt
}

type Params struct {
	Repositories *repositories.Repositories
	Logger       *logger.Logger
	Config       config.AppConfig
	Security     SecurityParams
}

func NewServices(p *Params) *Services {
	return &Services{
		Authentication: newAuthenticationService(p),
		Users:          newUserService(p),
		Roles:          newRoleService(p),
	}
}
