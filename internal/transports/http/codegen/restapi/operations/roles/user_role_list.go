// Code generated by go-swagger; DO NOT EDIT.

package roles

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// UserRoleListHandlerFunc turns a function with the right signature into a user role list handler
type UserRoleListHandlerFunc func(UserRoleListParams) middleware.Responder

// Handle executing the request and returning a response
func (fn UserRoleListHandlerFunc) Handle(params UserRoleListParams) middleware.Responder {
	return fn(params)
}

// UserRoleListHandler interface for that can handle valid user role list params
type UserRoleListHandler interface {
	Handle(UserRoleListParams) middleware.Responder
}

// NewUserRoleList creates a new http.Handler for the user role list operation
func NewUserRoleList(ctx *middleware.Context, handler UserRoleListHandler) *UserRoleList {
	return &UserRoleList{Context: ctx, Handler: handler}
}

/*UserRoleList swagger:route GET /api/roles Roles userRoleList

Get role list

*/
type UserRoleList struct {
	Context *middleware.Context
	Handler UserRoleListHandler
}

func (o *UserRoleList) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewUserRoleListParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
