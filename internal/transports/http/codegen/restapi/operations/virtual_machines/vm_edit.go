// Code generated by go-swagger; DO NOT EDIT.

package virtual_machines

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// VMEditHandlerFunc turns a function with the right signature into a vm edit handler
type VMEditHandlerFunc func(VMEditParams) middleware.Responder

// Handle executing the request and returning a response
func (fn VMEditHandlerFunc) Handle(params VMEditParams) middleware.Responder {
	return fn(params)
}

// VMEditHandler interface for that can handle valid vm edit params
type VMEditHandler interface {
	Handle(VMEditParams) middleware.Responder
}

// NewVMEdit creates a new http.Handler for the vm edit operation
func NewVMEdit(ctx *middleware.Context, handler VMEditHandler) *VMEdit {
	return &VMEdit{Context: ctx, Handler: handler}
}

/*VMEdit swagger:route PUT /api/vms/{id} Virtual machines vmEdit

Edit virtual machine

*/
type VMEdit struct {
	Context *middleware.Context
	Handler VMEditHandler
}

func (o *VMEdit) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewVMEditParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
