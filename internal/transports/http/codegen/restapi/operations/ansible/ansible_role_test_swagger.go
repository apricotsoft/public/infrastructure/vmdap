// Code generated by go-swagger; DO NOT EDIT.

package ansible

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// AnsibleRoleTestHandlerFunc turns a function with the right signature into a ansible role test handler
type AnsibleRoleTestHandlerFunc func(AnsibleRoleTestParams) middleware.Responder

// Handle executing the request and returning a response
func (fn AnsibleRoleTestHandlerFunc) Handle(params AnsibleRoleTestParams) middleware.Responder {
	return fn(params)
}

// AnsibleRoleTestHandler interface for that can handle valid ansible role test params
type AnsibleRoleTestHandler interface {
	Handle(AnsibleRoleTestParams) middleware.Responder
}

// NewAnsibleRoleTest creates a new http.Handler for the ansible role test operation
func NewAnsibleRoleTest(ctx *middleware.Context, handler AnsibleRoleTestHandler) *AnsibleRoleTest {
	return &AnsibleRoleTest{Context: ctx, Handler: handler}
}

/*AnsibleRoleTest swagger:route POST /api/ansible/test Ansible ansibleRoleTest

Ansible role testing

*/
type AnsibleRoleTest struct {
	Context *middleware.Context
	Handler AnsibleRoleTestHandler
}

func (o *AnsibleRoleTest) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewAnsibleRoleTestParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
