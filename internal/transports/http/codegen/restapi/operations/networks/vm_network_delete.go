// Code generated by go-swagger; DO NOT EDIT.

package networks

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// VMNetworkDeleteHandlerFunc turns a function with the right signature into a vm network delete handler
type VMNetworkDeleteHandlerFunc func(VMNetworkDeleteParams) middleware.Responder

// Handle executing the request and returning a response
func (fn VMNetworkDeleteHandlerFunc) Handle(params VMNetworkDeleteParams) middleware.Responder {
	return fn(params)
}

// VMNetworkDeleteHandler interface for that can handle valid vm network delete params
type VMNetworkDeleteHandler interface {
	Handle(VMNetworkDeleteParams) middleware.Responder
}

// NewVMNetworkDelete creates a new http.Handler for the vm network delete operation
func NewVMNetworkDelete(ctx *middleware.Context, handler VMNetworkDeleteHandler) *VMNetworkDelete {
	return &VMNetworkDelete{Context: ctx, Handler: handler}
}

/*VMNetworkDelete swagger:route DELETE /api/networks/{id} Networks vmNetworkDelete

Delete network

*/
type VMNetworkDelete struct {
	Context *middleware.Context
	Handler VMNetworkDeleteHandler
}

func (o *VMNetworkDelete) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewVMNetworkDeleteParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
