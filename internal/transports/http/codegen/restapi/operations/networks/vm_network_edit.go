// Code generated by go-swagger; DO NOT EDIT.

package networks

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// VMNetworkEditHandlerFunc turns a function with the right signature into a vm network edit handler
type VMNetworkEditHandlerFunc func(VMNetworkEditParams) middleware.Responder

// Handle executing the request and returning a response
func (fn VMNetworkEditHandlerFunc) Handle(params VMNetworkEditParams) middleware.Responder {
	return fn(params)
}

// VMNetworkEditHandler interface for that can handle valid vm network edit params
type VMNetworkEditHandler interface {
	Handle(VMNetworkEditParams) middleware.Responder
}

// NewVMNetworkEdit creates a new http.Handler for the vm network edit operation
func NewVMNetworkEdit(ctx *middleware.Context, handler VMNetworkEditHandler) *VMNetworkEdit {
	return &VMNetworkEdit{Context: ctx, Handler: handler}
}

/*VMNetworkEdit swagger:route PUT /api/networks/{id} Networks vmNetworkEdit

Edit network

*/
type VMNetworkEdit struct {
	Context *middleware.Context
	Handler VMNetworkEditHandler
}

func (o *VMNetworkEdit) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		r = rCtx
	}
	var Params = NewVMNetworkEditParams()

	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request

	o.Context.Respond(rw, r, route.Produces, route, res)

}
