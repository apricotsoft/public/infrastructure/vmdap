package middlewares

import (
	"net/http"
	"strings"
)

type SwaggerMiddleware struct{}

func newSwaggerMiddleware(_ *Params) *SwaggerMiddleware {
	return &SwaggerMiddleware{}
}

func (sm SwaggerMiddleware) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.URL.Path, "/swagger") {
			if strings.HasPrefix(r.RemoteAddr, "127.0.0.1") ||
				strings.HasPrefix(r.RemoteAddr, "[::1]") {
				http.FileServer(http.Dir("web")).ServeHTTP(w, r)
			} else {
				w.WriteHeader(404)

				return
			}
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
