package middlewares

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type FileMiddleware struct {
	assets string
}

func newFileMiddleware(p *Params) *FileMiddleware {
	return &FileMiddleware{
		assets: p.Config.Assets,
	}
}

func (fm FileMiddleware) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.URL.Path, "/api") {
			next.ServeHTTP(w, r)
		} else {
			if r.URL.Path == "/" || strings.Contains(r.URL.Path, ".") {
				http.FileServer(http.Dir(fm.assets)).ServeHTTP(w, r)
			} else {
				fm.sendIndex(w, r)
			}
		}
	})
}

func (fm FileMiddleware) sendIndex(rw http.ResponseWriter, r *http.Request) {
	f, err := os.Open(fmt.Sprintf("%s/index.html", fm.assets))

	defer func() {
		_ = f.Close()
	}()

	if err != nil {
		http.Redirect(rw, r, "/", http.StatusMovedPermanently)
		return
	}

	fileHeader := make([]byte, 512)
	_, _ = f.Read(fileHeader)
	fileContentType := http.DetectContentType(fileHeader)
	fileStat, _ := f.Stat()
	fileSize := strconv.FormatInt(fileStat.Size(), 10)

	rw.Header().Set("Content-Type", fileContentType)
	rw.Header().Set("Content-Length", fileSize)

	_, _ = f.Seek(0, 0)
	_, _ = io.Copy(rw, f)
}
