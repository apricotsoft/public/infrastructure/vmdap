package middlewares

import (
	"context"
	"github.com/go-openapi/runtime"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/security"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/models"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations/auth"
	"net/http"
	"strings"
)

type AuthenticationMiddleware struct {
	Jwt *security.Jwt
}

func newAuthenticationMiddleware(p *Params) *AuthenticationMiddleware {
	return &AuthenticationMiddleware{
		Jwt: p.Jwt,
	}
}

func (am AuthenticationMiddleware) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !strings.HasPrefix(r.URL.Path, "/api/auth") {
			bearer := strings.Split(
				r.Header.Get("Authorization"),
				" ",
			)

			if len(bearer) == 2 && bearer[1] != "" {
				payload, err := am.Jwt.Decode(bearer[1])

				if err != nil {
					if strings.Contains(err.Error(), "token expired") {
						auth.NewAuthSignInDefault(http.StatusUnauthorized).
							WithPayload(&models.Error{
								Code:    http.StatusUnauthorized,
								Message: "token expired",
							}).
							WriteResponse(w, runtime.JSONProducer())

						return
					}

					auth.NewAuthSignInDefault(http.StatusUnauthorized).
						WithPayload(&models.Error{
							Code:    http.StatusUnauthorized,
							Message: http.StatusText(http.StatusUnauthorized),
						}).
						WriteResponse(w, runtime.JSONProducer())

					return
				}

				if payload.Type != security.TypeAccess {
					auth.NewAuthSignInDefault(http.StatusUnauthorized).
						WithPayload(&models.Error{
							Code:    http.StatusUnauthorized,
							Message: http.StatusText(http.StatusUnauthorized),
						}).
						WriteResponse(w, runtime.JSONProducer())

					return
				}

				r = r.WithContext(context.WithValue(r.Context(), CtxUserKey, payload))
				next.ServeHTTP(w, r)

				return
			}

			auth.NewAuthSignInDefault(http.StatusUnauthorized).
				WithPayload(&models.Error{
					Code:    http.StatusUnauthorized,
					Message: http.StatusText(http.StatusUnauthorized),
				}).
				WriteResponse(w, runtime.JSONProducer())
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
