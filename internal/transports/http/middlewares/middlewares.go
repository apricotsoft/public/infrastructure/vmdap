package middlewares

import (
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/config"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/security"
	"net/http"
)

type ctxKey string

const (
	CtxUserKey ctxKey = "user"
)

type Middleware interface {
	Handler(next http.Handler) http.Handler
}

type Middlewares struct {
	File           Middleware
	Swagger        Middleware
	Authentication Middleware
}

type Params struct {
	Config config.AppWebConfig
	Jwt    *security.Jwt
}

func NewMiddlewares(p *Params) *Middlewares {
	return &Middlewares{
		File:           newFileMiddleware(p),
		Swagger:        newSwaggerMiddleware(p),
		Authentication: newAuthenticationMiddleware(p),
	}
}
