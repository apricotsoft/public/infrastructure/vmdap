package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/services"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/models"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations/auth"
)

type AuthenticationHandler struct {
	services *services.Services
	log      *logger.Log
}

func newAuthenticationHandler(
	s *services.Services,
	l *logger.Logger,
) *AuthenticationHandler {
	return &AuthenticationHandler{
		services: s,
		log:      l.GetLogger("authentication-handler"),
	}
}

func (ah AuthenticationHandler) SignIn() auth.AuthSignInHandlerFunc {
	return func(params auth.AuthSignInParams) middleware.Responder {
		ah.log.
			Info().
			Str("login", params.Body.Login).
			Str("ip", params.HTTPRequest.RemoteAddr).
			Msg("start handler login request")

		req := services.SignInRequest{
			Login:    params.Body.Login,
			Password: params.Body.Password,
		}
		req.Load(params.HTTPRequest)

		res, err := ah.services.Authentication.SignIn(params.HTTPRequest.Context(), req)

		if err != nil {
			ah.log.Warn().Err(err).Send()
			e := errorHandler(err)

			return auth.NewAuthSignInDefault(int(e.Code)).WithPayload(e)
		}

		return &auth.AuthSignInOK{
			Payload: &models.AuthResponse{
				Access:  res.Access,
				Refresh: res.Refresh,
			},
		}
	}
}

func (ah AuthenticationHandler) Exchange() auth.AuthExchangeHandlerFunc {
	return func(params auth.AuthExchangeParams) middleware.Responder {
		ah.log.
			Info().
			Str("token", params.Body.Token).
			Str("ip", params.HTTPRequest.RemoteAddr).
			Msg("start handler exchange token request")

		req := services.ExchangeRequest{
			Token: params.Body.Token,
		}
		req.Load(params.HTTPRequest)

		res, err := ah.services.Authentication.Exchange(params.HTTPRequest.Context(), req)

		if err != nil {
			ah.log.Warn().Err(err).Send()
			e := errorHandler(err)

			return auth.NewAuthExchangeDefault(int(e.Code)).WithPayload(e)
		}

		return &auth.AuthExchangeOK{
			Payload: &models.AuthResponse{
				Access:  res.Access,
				Refresh: res.Refresh,
			},
		}
	}
}
