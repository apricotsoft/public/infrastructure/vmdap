package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/services"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations/dashboard"
)

type DashboardHandler struct {
	services *services.Services
	log      *logger.Log
}

func newDashboardHandler(
	s *services.Services,
	l *logger.Logger,
) *DashboardHandler {
	return &DashboardHandler{
		services: s,
		log:      l.GetLogger("dashboard-handler"),
	}
}

func (dh DashboardHandler) Info() dashboard.DashboardGetHandlerFunc {
	return func(params dashboard.DashboardGetParams) middleware.Responder {
		dh.log.
			Info().
			Msg("start handler info request")

		return dashboard.NewDashboardGetDefault(200)
	}
}
