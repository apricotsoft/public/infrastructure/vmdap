package handlers

import (
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/constants"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/services"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/models"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations/auth"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations/dashboard"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations/roles"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations/users"
	"net/http"
)

type Dashboard interface {
	Info() dashboard.DashboardGetHandlerFunc
}

type Role interface {
	List() roles.UserRoleListHandlerFunc
}

type Authentication interface {
	SignIn() auth.AuthSignInHandlerFunc
	Exchange() auth.AuthExchangeHandlerFunc
}

type User interface {
	GetById() users.UserGetHandlerFunc
	GetSelf() users.UserGetSelfHandlerFunc
}

type Handlers struct {
	Authentication Authentication
	Dashboard      Dashboard
	User           User
	Role           Role
}

func NewHandlers(s *services.Services, l *logger.Logger) *Handlers {
	return &Handlers{
		Authentication: newAuthenticationHandler(s, l),
		Dashboard:      newDashboardHandler(s, l),
		User:           newUserHandler(s, l),
		Role:           newRoleHandler(s, l),
	}
}

func errorHandler(err error) *models.Error {
	switch e := err.(type) {
	case *services.ServiceError:
		return &models.Error{
			Code:    e.GetCode(),
			Details: e.GetDetail(),
			Message: e.GetMessage(),
		}
	default:
		return &models.Error{
			Code:    http.StatusInternalServerError,
			Message: string(constants.InternalServerError),
		}
	}
}
