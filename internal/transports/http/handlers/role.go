package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/repositories"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/services"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/models"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations/roles"
)

type RoleHandler struct {
	services *services.Services
	log      *logger.Log
}

func newRoleHandler(
	s *services.Services,
	l *logger.Logger,
) *RoleHandler {
	return &RoleHandler{
		services: s,
		log:      l.GetLogger("role-handler"),
	}
}

func (rh RoleHandler) List() roles.UserRoleListHandlerFunc {
	return func(params roles.UserRoleListParams) middleware.Responder {
		rh.log.
			Info().
			Msg("start role list request")

		limit := uint32(20)
		skip := uint32(0)
		order := repositories.OrderDesc
		sort := "id"

		if params.Limit != nil {
			limit = uint32(*params.Limit)
		}

		if params.Skip != nil {
			skip = uint32(*params.Skip)
		}

		if params.Sort != nil {
			sort = *params.Sort
		}

		if params.Order != nil {
			switch *params.Order {
			case -1:
				order = repositories.OrderDesc
			case 1:
				order = repositories.OrderAsc
			case 0:
				order = repositories.OrderNone
			}
		}

		req := services.RoleListRequest{
			Limit: limit,
			Skip:  skip,
			Sort:  sort,
			Order: order,
			Roles: params.Names,
		}
		req.Load(params.HTTPRequest)

		result, err := rh.services.Roles.List(params.HTTPRequest.Context(), req)

		if err != nil {
			rh.log.Warn().Err(err).Send()
			e := errorHandler(err)

			return roles.NewUserRoleListDefault(int(e.Code)).WithPayload(e)
		}

		res := &models.RoleListResponse{
			Result: make([]*models.Role, 0, len(result.Result)),
		}

		for _, r := range result.Result {
			role := &models.Role{
				Actions:     r.Actions,
				CreatedAt:   float64(r.CreatedAt.Unix() * 1000),
				Description: r.Description,
				ID:          int64(r.Id),
				Name:        r.Name,
				UpdatedAt:   float64(r.UpdatedAt.Unix() * 1000),
			}

			if r.DeletedAt != nil {
				role.DeletedAt = float64(r.DeletedAt.Unix() * 1000)
			}

			res.Result = append(res.Result, role)
		}

		if result.Next != nil {
			res.Next = &models.RoleListRequest{
				Limit: int64(result.Next.Limit),
				Names: result.Next.Roles,
				Skip:  int64(result.Next.Skip),
				Sort:  result.Next.Sort,
			}

			switch result.Next.Order {
			case repositories.OrderNone:
				res.Next.Order = 0
			case repositories.OrderAsc:
				res.Next.Order = 1
			case repositories.OrderDesc:
				res.Next.Order = -1
			}
		}

		return roles.NewUserRoleListOK().WithPayload(res)
	}
}
