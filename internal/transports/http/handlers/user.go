package handlers

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/services"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/models"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations/users"
)

type UserHandler struct {
	services *services.Services
	log      *logger.Log
}

func newUserHandler(
	s *services.Services,
	l *logger.Logger,
) *UserHandler {
	return &UserHandler{
		services: s,
		log:      l.GetLogger("user-handler"),
	}
}

func (uh UserHandler) GetById() users.UserGetHandlerFunc {
	return func(params users.UserGetParams) middleware.Responder {
		uh.log.
			Info().
			Msg("start get user by id request")

		req := services.GetUserByIdRequest{}
		req.Load(params.HTTPRequest)

		//res, err := ah.services.Authentication.SignIn(params.HTTPRequest.Context(), req)
		//
		//if err != nil {
		//	ah.log.Warn().Err(err).Send()
		//	e := errorHandler(err)
		//
		//	return auth.NewAuthSignInDefault(int(e.Code)).WithPayload(e)
		//}

		return users.NewUserGetDefault(400).WithPayload(&models.Error{
			Code:    400,
			Details: "invalid details invalid details invalid details invalid details invalid details",
			Message: "bay request",
		})
		//return users.NewUserGetOK()
	}
}

func (uh UserHandler) GetSelf() users.UserGetSelfHandlerFunc {
	return func(params users.UserGetSelfParams) middleware.Responder {
		uh.log.
			Info().
			Msg("start get self user request")

		req := services.GetSelfUserRequest{}
		req.Load(params.HTTPRequest)

		res, err := uh.services.Users.GetSelf(params.HTTPRequest.Context(), req)

		if err != nil {
			uh.log.Warn().Err(err).Send()
			e := errorHandler(err)

			return users.NewUserGetSelfDefault(int(e.Code)).WithPayload(e)
		}

		roles := make([]string, 0, len(res.Roles))

		for _, role := range res.Roles {
			roles = append(roles, role.Name)
		}

		return users.NewUserGetSelfOK().
			WithPayload(
				&models.SelfUserResponse{
					Date:  float64(res.Date.Unix() * 1000),
					ID:    int64(res.Id),
					Login: res.Login,
					Roles: roles,
				},
			)
	}
}
