package server

import (
	"context"
	"github.com/go-openapi/errors"
	"github.com/go-openapi/loads"
	"github.com/jessevdk/go-flags"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/config"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/security"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/services"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/codegen/restapi/operations"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/handlers"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/transports/http/middlewares"
	"net/http"
)

type Server struct {
	spec        *loads.Document
	server      *restapi.Server
	api         *operations.VMDAPAPI
	logger      *logger.Log
	config      *config.Config
	services    *services.Services
	middlewares *middlewares.Middlewares
	handlers    *handlers.Handlers
}

func NewServer(
	services *services.Services,
	logger *logger.Logger,
	cfg *config.Config,
	jwt *security.Jwt,
) (*Server, error) {
	log := logger.GetLogger("http-server")
	swaggerSpec, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)

	if err != nil {
		log.Error().Err(err).Send()

		return nil, err
	}

	api := operations.NewVMDAPAPI(swaggerSpec)
	api.Logger = func(s string, i ...interface{}) {
		log.Info().Msgf(s, i...)
	}

	return &Server{
		spec:   swaggerSpec,
		api:    api,
		server: restapi.NewServer(api),
		logger: log,
		config: cfg,
		middlewares: middlewares.NewMiddlewares(&middlewares.Params{
			Config: cfg.App.Web,
			Jwt:    jwt,
		}),
		handlers: handlers.NewHandlers(services, logger),
	}, nil
}

func (s *Server) Start(ctx context.Context) error {
	parser := flags.NewParser(s.server, flags.Default)
	parser.ShortDescription = s.spec.Spec().Info.Title
	parser.LongDescription = s.spec.Spec().Info.Description
	s.server.ConfigureFlags()

	for _, optsGroup := range s.api.CommandLineOptionsGroups {
		_, err := parser.AddGroup(optsGroup.ShortDescription, optsGroup.LongDescription, optsGroup.Options)

		if err != nil {
			s.logger.Error().Err(err).Send()

			return err
		}
	}

	s.api.ServeError = func(rw http.ResponseWriter, r *http.Request, err error) {
		errors.ServeError(rw, r, err)
	}

	s.bind()

	s.server.ConfigureAPI()
	s.server.SetHandler(
		s.middlewares.Swagger.Handler(
			s.middlewares.File.Handler(
				s.middlewares.Authentication.Handler(
					s.server.GetHandler(),
				),
			),
		),
	)

	s.server.EnabledListeners = []string{"http"}
	s.server.Host = s.config.Listen.Host
	s.server.Port = s.config.Listen.Ports.Http

	if s.config.Listen.Ssl.Enable {
		s.server.EnabledListeners = []string{"https"}
		s.server.TLSCertificate = flags.Filename(s.config.Listen.Ssl.Certificate)
		s.server.TLSCertificateKey = flags.Filename(s.config.Listen.Ssl.Key)
		s.server.TLSHost = s.config.Listen.Host
		s.server.TLSPort = s.config.Listen.Ports.Http
	}

	err := s.server.Serve()

	if err != nil {
		s.logger.Error().Err(err).Send()

		return err
	}

	return nil
}

func (s *Server) Stop() error {
	return s.server.Shutdown()
}

func (s Server) bind() {
	s.api.AuthAuthSignInHandler = s.handlers.Authentication.SignIn()
	s.api.AuthAuthExchangeHandler = s.handlers.Authentication.Exchange()

	s.api.UsersUserGetHandler = s.handlers.User.GetById()
	s.api.UsersUserGetSelfHandler = s.handlers.User.GetSelf()

	s.api.DashboardDashboardGetHandler = s.handlers.Dashboard.Info()

	s.api.RolesUserRoleListHandler = s.handlers.Role.List()
}
