package repositories

import (
	"context"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/sqlite"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"strings"
	"time"
)

const roleActionsTable = "t_role_actions"

type RoleActionsRepo struct {
	sqlite *sqlite.Sqlite
	logger *logger.Log
}

func newRoleActionsRepo(
	sqlite *sqlite.Sqlite,
	logger *logger.Logger,
) *RoleActionsRepo {
	return &RoleActionsRepo{
		sqlite: sqlite,
		logger: logger.GetLogger("role-actions-repo"),
	}
}

func (rar RoleActionsRepo) GetByRoleIds(ctx context.Context, roleIds []uint64) ([]*domains.RoleAction, error) {
	inQuery := make([]string, 0, len(roleIds))
	args := make([]interface{}, 0, len(roleIds))

	for _, id := range roleIds {
		inQuery = append(inQuery, "?")
		args = append(args, id)
	}

	rows, err := rar.sqlite.GetClient().QueryContext(
		ctx,
		fmt.Sprintf(
			`
				SELECT 
					id,
					role_id,
					action_id,
					created_at,
					deleted_at
				FROM %s
				WHERE role_id IN (%s);
			`,
			roleActionsTable,
			strings.Join(inQuery, ", "),
		),
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf(
			"error getting actionIds by roleIds: %w",
			err,
		)
	}

	defer func() {
		err = rows.Close()

		if err != nil {
			rar.logger.Error().Err(err).Send()
		}
	}()

	result := make([]*domains.RoleAction, 0, 100)

	for rows.Next() {
		ra := &domains.RoleAction{}
		var createdAt int64
		var deletedAt *int64
		err = rows.Scan(&ra.Id, &ra.RoleId, &ra.ActionId, &createdAt, &deletedAt)

		if err != nil {
			return nil, fmt.Errorf(
				"error scan user role row: %w",
				err,
			)
		}

		ra.CreatedAt = time.Unix(0, createdAt*int64(time.Nanosecond))

		if deletedAt != nil {
			ra.DeletedAt = time.Unix(0, *deletedAt*int64(time.Nanosecond))
		}

		result = append(result, ra)
	}

	return result, nil
}
