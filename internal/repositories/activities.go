package repositories

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	bolt2 "github.com/boltdb/bolt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/bolt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"time"
)

const activitiesBucket = "activities"

type activity struct {
	Endpoint  string                 `json:"endpoint"`
	Method    domains.ActivityMethod `json:"method"`
	SessionId string                 `json:"session_id"`
	ActionId  int64                  `json:"action_id,omitempty"`
}

type ActivitiesRepo struct {
	bolt   *bolt.Bolt
	logger *logger.Log
}

func newActivitiesRepo(
	bolt *bolt.Bolt,
	logger *logger.Logger,
) *ActivitiesRepo {
	return &ActivitiesRepo{
		bolt:   bolt,
		logger: logger.GetLogger("activities-repo"),
	}
}

func (ar ActivitiesRepo) Set(params domains.Activity) error {
	return ar.bolt.GetClient().Update(func(tx *bolt2.Tx) error {
		activityBucket := tx.Bucket([]byte(activitiesBucket))
		userBucket := make([]byte, 8)
		binary.LittleEndian.PutUint64(userBucket, params.UserId)
		b, err := activityBucket.CreateBucketIfNotExists(userBucket)

		if err != nil {
			return fmt.Errorf(
				"error from create: %w",
				err,
			)
		}

		a := &activity{
			Endpoint:  params.Endpoint,
			Method:    params.Method,
			SessionId: params.SessionId,
		}

		if params.ActionId != nil {
			a.ActionId = *params.ActionId
		}

		value, err := json.Marshal(a)

		if err != nil {
			return fmt.Errorf(
				"error from serialze activity: %w",
				err,
			)
		}

		key := make([]byte, 8)
		binary.LittleEndian.PutUint64(key, uint64(time.Now().UTC().UnixNano()))

		err = b.Put(key, value)

		if err != nil {
			return fmt.Errorf(
				"error from put activity: %w",
				err,
			)
		}

		return nil
	})
}
