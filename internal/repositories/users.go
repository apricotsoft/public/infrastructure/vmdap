package repositories

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/sqlite"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"time"
)

const usersTable = "t_users"

type UsersRepo struct {
	sqlite *sqlite.Sqlite
	logger *logger.Log
}

func newUsersRepo(
	sqlite *sqlite.Sqlite,
	logger *logger.Logger,
) *UsersRepo {
	return &UsersRepo{
		sqlite: sqlite,
		logger: logger.GetLogger("users-repo"),
	}
}

func (ur UsersRepo) GetByLogin(ctx context.Context, login string) (*domains.User, error) {
	result := ur.sqlite.GetClient().QueryRowContext(
		ctx,
		fmt.Sprintf(
			`
				SELECT 
					id,
					login,
					password,
					created_at,
					updated_at
				FROM %s
				WHERE login = ?;
			`,
			usersTable,
		),
		login,
	)

	if result.Err() != nil {
		return nil, fmt.Errorf(
			"error getting user by login filed: %w",
			result.Err(),
		)
	}

	u := &domains.User{}
	var createdAt, updatedAt int64
	err := result.Scan(&u.Id, &u.Login, &u.Password, &createdAt, &updatedAt)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}

		return nil, fmt.Errorf(
			"error scan user row: %w",
			err,
		)
	}

	u.CreatedAt = time.Unix(0, createdAt*int64(time.Nanosecond))
	u.UpdatedAt = time.Unix(0, updatedAt*int64(time.Nanosecond))

	return u, nil
}

func (ur UsersRepo) GetById(ctx context.Context, id uint64) (*domains.User, error) {
	result := ur.sqlite.GetClient().QueryRowContext(
		ctx,
		fmt.Sprintf(
			`
				SELECT 
					id,
					login,
					password,
					created_at,
					updated_at
				FROM %s
				WHERE id = ?;
			`,
			usersTable,
		),
		id,
	)

	if result.Err() != nil {
		return nil, fmt.Errorf(
			"error getting user by id filed: %w",
			result.Err(),
		)
	}

	u := &domains.User{}
	var createdAt, updatedAt int64
	err := result.Scan(&u.Id, &u.Login, &u.Password, &createdAt, &updatedAt)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}

		return nil, fmt.Errorf(
			"error scan user row: %w",
			err,
		)
	}

	u.CreatedAt = time.Unix(0, createdAt*int64(time.Nanosecond))
	u.UpdatedAt = time.Unix(0, updatedAt*int64(time.Nanosecond))

	return u, nil
}
