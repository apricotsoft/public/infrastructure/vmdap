package repositories

import (
	"context"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/sqlite"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"time"
)

const userRolesTable = "t_user_roles"

type UserRolesRepo struct {
	sqlite *sqlite.Sqlite
	logger *logger.Log
}

func newUserRolesRepo(
	sqlite *sqlite.Sqlite,
	logger *logger.Logger,
) *UserRolesRepo {
	return &UserRolesRepo{
		sqlite: sqlite,
		logger: logger.GetLogger("user-roles-repo"),
	}
}

func (urr UserRolesRepo) GetByUserId(ctx context.Context, userId uint64) ([]*domains.UserRole, error) {
	rows, err := urr.sqlite.GetClient().QueryContext(
		ctx,
		fmt.Sprintf(
			`
				SELECT 
					id,
					user_id,
					role_id,
					created_at,
					deleted_at
				FROM %s
				WHERE user_id = ?;
			`,
			userRolesTable,
		),
		userId,
	)

	if err != nil {
		return nil, fmt.Errorf(
			"error getting roleIds by userId: %w",
			err,
		)
	}

	defer func() {
		err = rows.Close()

		if err != nil {
			urr.logger.Error().Err(err).Send()
		}
	}()

	result := make([]*domains.UserRole, 0, 100)

	for rows.Next() {
		ur := &domains.UserRole{}
		var createdAt int64
		var deletedAt *int64
		err = rows.Scan(&ur.Id, &ur.UserId, &ur.RoleId, &createdAt, &deletedAt)

		if err != nil {
			return nil, fmt.Errorf(
				"error scan user role row: %w",
				err,
			)
		}

		ur.CreatedAt = time.Unix(0, createdAt*int64(time.Nanosecond))

		if deletedAt != nil {
			ur.DeletedAt = time.Unix(0, *deletedAt*int64(time.Nanosecond))
		}

		result = append(result, ur)
	}

	return result, nil
}
