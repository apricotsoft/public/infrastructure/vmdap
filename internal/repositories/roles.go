package repositories

import (
	"context"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/sqlite"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"strings"
	"time"
)

const rolesTable = "t_roles"

type RolesRepo struct {
	sqlite *sqlite.Sqlite
	logger *logger.Log
}

func newRolesRepo(
	sqlite *sqlite.Sqlite,
	logger *logger.Logger,
) *RolesRepo {
	return &RolesRepo{
		sqlite: sqlite,
		logger: logger.GetLogger("roles-repo"),
	}
}

func (rr RolesRepo) GetByIds(ctx context.Context, ids []uint64) ([]*domains.Role, error) {
	inQuery := make([]string, 0, len(ids))
	args := make([]interface{}, 0, len(ids))

	for _, id := range ids {
		inQuery = append(inQuery, "?")
		args = append(args, id)
	}

	rows, err := rr.sqlite.GetClient().QueryContext(
		ctx,
		fmt.Sprintf(
			`
				SELECT 
					id,
					name,
					description,
					created_at,
					updated_at,
					deleted_at
				FROM %s
				WHERE id IN (%s);
			`,
			rolesTable,
			strings.Join(inQuery, ", "),
		),
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf(
			"error getting roles by ids: %w",
			err,
		)
	}

	defer func() {
		err = rows.Close()

		if err != nil {
			rr.logger.Error().Err(err).Send()
		}
	}()

	result := make([]*domains.Role, 0, 100)

	for rows.Next() {
		r := &domains.Role{}
		var createdAt, updatedAt int64
		var deletedAt *int64
		err = rows.Scan(&r.Id, &r.Name, &r.Description, &createdAt, &updatedAt, &deletedAt)

		if err != nil {
			return nil, fmt.Errorf(
				"error scan role row: %w",
				err,
			)
		}

		r.CreatedAt = time.Unix(0, createdAt*int64(time.Nanosecond))
		r.UpdatedAt = time.Unix(0, updatedAt*int64(time.Nanosecond))

		if deletedAt != nil {
			dt := time.Unix(0, *deletedAt*int64(time.Nanosecond))
			r.DeletedAt = &dt
		}

		result = append(result, r)
	}

	return result, nil
}

func (rr RolesRepo) GetByNames(ctx context.Context, names []string) ([]*domains.Role, error) {
	inQuery := make([]string, 0, len(names))
	args := make([]interface{}, 0, len(names))

	for _, id := range names {
		inQuery = append(inQuery, "?")
		args = append(args, id)
	}

	rows, err := rr.sqlite.GetClient().QueryContext(
		ctx,
		fmt.Sprintf(
			`
				SELECT 
					id,
					name,
					description,
					created_at,
					updated_at,
					deleted_at
				FROM %s
				WHERE name IN (%s);
			`,
			rolesTable,
			strings.Join(inQuery, ", "),
		),
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf(
			"error getting roles by names: %w",
			err,
		)
	}

	defer func() {
		err = rows.Close()

		if err != nil {
			rr.logger.Error().Err(err).Send()
		}
	}()

	result := make([]*domains.Role, 0, 100)

	for rows.Next() {
		r := &domains.Role{}
		var createdAt, updatedAt int64
		var deletedAt *int64
		err = rows.Scan(&r.Id, &r.Name, &r.Description, &createdAt, &updatedAt, &deletedAt)

		if err != nil {
			return nil, fmt.Errorf(
				"error scan role row: %w",
				err,
			)
		}

		r.CreatedAt = time.Unix(0, createdAt*int64(time.Nanosecond))
		r.UpdatedAt = time.Unix(0, updatedAt*int64(time.Nanosecond))

		if deletedAt != nil {
			dt := time.Unix(0, *deletedAt*int64(time.Nanosecond))
			r.DeletedAt = &dt
		}

		result = append(result, r)
	}

	return result, nil
}

func (rr RolesRepo) Get(
	ctx context.Context,
	limit, skip uint32,
	sort string,
	order Order,
) ([]*domains.Role, error) {
	args := make([]interface{}, 0, 2)
	limitQuery := ""
	skipQuery := ""
	sortQuery := ""

	if sort != "" || order == OrderNone {
		sortQuery = fmt.Sprintf(
			`ORDER BY %s %s`,
			sort,
			order,
		)
	}

	if limit != 0 {
		limitQuery = `Limit ?`
		args = append(args, limit)
	}

	if skip != 0 {
		limitQuery = `OFFSET ?`
		args = append(args, skip)
	}

	query := fmt.Sprintf(
		`
				SELECT 
					id,
					name,
					description,
					created_at,
					updated_at,
					deleted_at
				FROM %s
				%s
				%s
				%s;
			`,
		rolesTable,
		sortQuery,
		limitQuery,
		skipQuery,
	)

	rows, err := rr.sqlite.GetClient().QueryContext(
		ctx,
		query,
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf(
			"error getting roles: %w",
			err,
		)
	}

	defer func() {
		err = rows.Close()

		if err != nil {
			rr.logger.Error().Err(err).Send()
		}
	}()

	resultLength := uint32(100)

	if limit > resultLength {
		resultLength = limit
	}

	result := make([]*domains.Role, 0, resultLength)

	for rows.Next() {
		r := &domains.Role{}
		var createdAt, updatedAt int64
		var deletedAt *int64
		err = rows.Scan(&r.Id, &r.Name, &r.Description, &createdAt, &updatedAt, &deletedAt)

		if err != nil {
			return nil, fmt.Errorf(
				"error scan role row: %w",
				err,
			)
		}

		r.CreatedAt = time.Unix(0, createdAt*int64(time.Nanosecond))
		r.UpdatedAt = time.Unix(0, updatedAt*int64(time.Nanosecond))

		if deletedAt != nil {
			dt := time.Unix(0, *deletedAt*int64(time.Nanosecond))
			r.DeletedAt = &dt
		}

		result = append(result, r)
	}

	return result, nil
}
