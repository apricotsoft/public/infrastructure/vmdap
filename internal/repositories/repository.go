package repositories

import (
	"context"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/bolt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/sqlite"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
)

type Order string

const (
	OrderNone Order = ""
	OrderAsc  Order = "ASC"
	OrderDesc Order = "DESC"
)

type Users interface {
	GetByLogin(ctx context.Context, login string) (*domains.User, error)
	GetById(ctx context.Context, id uint64) (*domains.User, error)
}

type Roles interface {
	GetByIds(ctx context.Context, ids []uint64) ([]*domains.Role, error)
	GetByNames(ctx context.Context, names []string) ([]*domains.Role, error)
	Get(ctx context.Context, limit, skip uint32, sort string, order Order) ([]*domains.Role, error)
}

type Actions interface {
	GetByIds(ctx context.Context, ids []uint64) ([]*domains.Action, error)
}

type UserRoles interface {
	GetByUserId(ctx context.Context, userId uint64) ([]*domains.UserRole, error)
}

type RoleActions interface {
	GetByRoleIds(ctx context.Context, roleIds []uint64) ([]*domains.RoleAction, error)
}

type Sessions interface {
	Create(ctx context.Context, id, ip string, userId uint64, expire int64) error
}

type Activities interface {
	Set(params domains.Activity) error
}

type Tokens interface {
	Set(token, sessionId string) error
	Get(token string) string
	Delete(token string) error
}

type Repositories struct {
	Users       Users
	Sessions    Sessions
	Activities  Activities
	Tokens      Tokens
	Roles       Roles
	UserRoles   UserRoles
	RoleActions RoleActions
	Actions     Actions
}

func NewRepositories(
	sqlite *sqlite.Sqlite,
	bolt *bolt.Bolt,
	logger *logger.Logger,
) *Repositories {
	return &Repositories{
		Users:       newUsersRepo(sqlite, logger),
		Sessions:    newSessionsRepo(sqlite, logger),
		Activities:  newActivitiesRepo(bolt, logger),
		Tokens:      newTokensRepo(bolt, logger),
		Roles:       newRolesRepo(sqlite, logger),
		UserRoles:   newUserRolesRepo(sqlite, logger),
		RoleActions: newRoleActionsRepo(sqlite, logger),
		Actions:     newActionsRepo(sqlite, logger),
	}
}
