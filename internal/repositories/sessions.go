package repositories

import (
	"context"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/sqlite"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"time"
)

const sessionsTable = "t_sessions"

type SessionsRepo struct {
	sqlite *sqlite.Sqlite
	logger *logger.Log
}

func newSessionsRepo(
	sqlite *sqlite.Sqlite,
	logger *logger.Logger,
) *SessionsRepo {
	return &SessionsRepo{
		sqlite: sqlite,
		logger: logger.GetLogger("sessions-repo"),
	}
}

func (sr SessionsRepo) Create(ctx context.Context, id, ip string, userId uint64, expire int64) error {
	stmt, err := sr.sqlite.GetClient().PrepareContext(
		ctx,
		fmt.Sprintf(
			`
				INSERT INTO %s(id, user_id, ip, created_at, expired_at)
				VALUES(?, ?, ?, ?, ?);
			`,
			sessionsTable,
		),
	)

	if err != nil {
		return fmt.Errorf("error prepare insert session query: %w", err)
	}

	_, err = stmt.ExecContext(
		ctx,
		id,
		userId,
		ip,
		time.Now().UTC().UnixNano(),
		expire,
	)

	if err != nil {
		return fmt.Errorf("error exec insert session query: %w", err)
	}

	return nil
}
