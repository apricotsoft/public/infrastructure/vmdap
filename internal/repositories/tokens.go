package repositories

import (
	"encoding/binary"
	"fmt"
	bolt2 "github.com/boltdb/bolt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/bolt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"time"
)

type TokensRepo struct {
	bolt            *bolt.Bolt
	logger          *logger.Log
	bucket          []byte
	ttlSubBucket    []byte
	valuesSubBucket []byte
}

func newTokensRepo(
	bolt *bolt.Bolt,
	logger *logger.Logger,
) *TokensRepo {
	return &TokensRepo{
		bolt:            bolt,
		logger:          logger.GetLogger("tokens-repo"),
		bucket:          []byte("tokens"),
		ttlSubBucket:    []byte("ttl"),
		valuesSubBucket: []byte("values"),
	}
}

func (tr TokensRepo) Set(token, sessionId string) error {
	return tr.bolt.GetClient().Update(func(tx *bolt2.Tx) error {
		root := tx.Bucket(tr.bucket)
		valuesBucket := root.Bucket(tr.valuesSubBucket)
		ttlBucket := root.Bucket(tr.ttlSubBucket)

		ttlKey := make([]byte, 8)
		binary.LittleEndian.PutUint64(ttlKey, uint64(time.Now().UTC().UnixNano()))
		key := []byte(token)

		err := ttlBucket.Put(ttlKey, key)

		if err != nil {
			return fmt.Errorf(
				"error from put token ttl key: %w",
				err,
			)
		}

		err = valuesBucket.Put(key, []byte(sessionId))

		if err != nil {
			return fmt.Errorf(
				"error from put token value: %w",
				err,
			)
		}

		return nil
	})
}

func (tr TokensRepo) Get(token string) string {
	sessionId := ""

	_ = tr.bolt.GetClient().View(func(tx *bolt2.Tx) error {
		root := tx.Bucket(tr.bucket)
		valuesBucket := root.Bucket(tr.valuesSubBucket)
		sessionId = string(valuesBucket.Get([]byte(token)))

		return nil
	})

	return sessionId
}

func (tr TokensRepo) Delete(token string) error {
	return tr.bolt.GetClient().Update(func(tx *bolt2.Tx) error {
		root := tx.Bucket(tr.bucket)
		valuesBucket := root.Bucket(tr.valuesSubBucket)

		err := valuesBucket.Delete([]byte(token))

		if err != nil {
			return fmt.Errorf(
				"error from delete token: %w",
				err,
			)
		}

		return nil
	})
}
