package repositories

import (
	"context"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/domains"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/sqlite"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"strings"
	"time"
)

const actionsTable = "t_actions"

type ActionsRepo struct {
	sqlite *sqlite.Sqlite
	logger *logger.Log
}

func newActionsRepo(
	sqlite *sqlite.Sqlite,
	logger *logger.Logger,
) *ActionsRepo {
	return &ActionsRepo{
		sqlite: sqlite,
		logger: logger.GetLogger("actions-repo"),
	}
}

func (rr ActionsRepo) GetByIds(ctx context.Context, ids []uint64) ([]*domains.Action, error) {
	inQuery := make([]string, 0, len(ids))
	args := make([]interface{}, 0, len(ids))

	for _, id := range ids {
		inQuery = append(inQuery, "?")
		args = append(args, id)
	}

	rows, err := rr.sqlite.GetClient().QueryContext(
		ctx,
		fmt.Sprintf(
			`
				SELECT 
					id,
					name,
					description,
					created_at,
					updated_at,
					deleted_at
				FROM %s
				WHERE id IN (%s);
			`,
			actionsTable,
			strings.Join(inQuery, ", "),
		),
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf(
			"error getting roles by ids: %w",
			err,
		)
	}

	defer func() {
		err = rows.Close()

		if err != nil {
			rr.logger.Error().Err(err).Send()
		}
	}()

	result := make([]*domains.Action, 0, 100)

	for rows.Next() {
		a := &domains.Action{}
		var createdAt, updatedAt int64
		var deletedAt *int64
		err = rows.Scan(&a.Id, &a.Name, &a.Description, &createdAt, &updatedAt, &deletedAt)

		if err != nil {
			return nil, fmt.Errorf(
				"error scan role row: %w",
				err,
			)
		}

		a.CreatedAt = time.Unix(0, createdAt*int64(time.Nanosecond))
		a.UpdatedAt = time.Unix(0, updatedAt*int64(time.Nanosecond))

		if deletedAt != nil {
			a.DeletedAt = time.Unix(0, *deletedAt*int64(time.Nanosecond))
		}

		result = append(result, a)
	}

	return result, nil
}
