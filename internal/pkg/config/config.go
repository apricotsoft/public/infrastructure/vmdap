package config

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v3"
	"os"
	"reflect"
	"time"
)

type AppWebConfig struct {
	Assets string `yaml:"assets" envconfig:"VMDAP_APP_WEB_ASSETS"`
}

type AppUsersRootConfig struct {
	Password string `yaml:"pass" envconfig:"VMDAP_APP_USERS_ROOT_PASS"`
}

type AppUsersConfig struct {
	Root AppUsersRootConfig `yaml:"root"`
}

type AppSecurityJwtExpiresConfig struct {
	Access  time.Duration `yaml:"access" envconfig:"VMDAP_APP_SECURITY_JWT_EXPIRES_ACCESS"`
	Refresh time.Duration `yaml:"refresh" envconfig:"VMDAP_APP_SECURITY_JWT_EXPIRES_REFRESH"`
}

type AppSecurityJwtConfig struct {
	Expires   AppSecurityJwtExpiresConfig `yaml:"expires"`
	Secret    string                      `yaml:"secret" envconfig:"VMDAP_APP_SECURITY_JWT_SECRET"`
	Algorithm string                      `yaml:"algorithm" envconfig:"VMDAP_APP_SECURITY_JWT_ALGORITHM"`
}

type AppSecurityConfig struct {
	Cost int                  `yaml:"cost" envconfig:"VMDAP_APP_SECURITY_COST"`
	Jwt  AppSecurityJwtConfig `yaml:"jwt"`
}

type AppConfig struct {
	Web      AppWebConfig      `yaml:"web"`
	Users    AppUsersConfig    `yaml:"users"`
	Security AppSecurityConfig `yaml:"security"`
}

type PortsConfig struct {
	Http   int `yaml:"http" envconfig:"VMDAP_LISTEN_HTTP_PORT"`
	Socket int `yaml:"socket" envconfig:"VMDAP_LISTEN_GRPC_PORT"`
}

type ListenSslConfig struct {
	Enable      bool   `yaml:"enable" envconfig:"VMDAP_LISTEN_SSL_ENABLE"`
	Key         string `yaml:"key" envconfig:"VMDAP_LISTEN_SSL_KEY"`
	Certificate string `yaml:"crt" envconfig:"VMDAP_LISTEN_SSL_CRT"`
}

type ListenConfig struct {
	Ports PortsConfig     `yaml:"ports"`
	Host  string          `yaml:"host" envconfig:"VMDAP_LISTEN_HOST"`
	Ssl   ListenSslConfig `yaml:"ssl"`
}

type SqliteOptionConfig struct {
	JournalMode string `yaml:"journal_mode" envconfig:"VMDAP_DB_SQLITE_OPTION_JOURNAL_MODE"`
	Synchronous string `yaml:"synchronous" envconfig:"VMDAP_DB_SQLITE_OPTION_SYNCHRONOUS"`
	TempStore   string `yaml:"temp_store" envconfig:"VMDAP_DB_SQLITE_OPTION_TEMP_STORE"`
	MmapSize    int64  `yaml:"mmap_size" envconfig:"VMDAP_DB_SQLITE_OPTION_MMAP_SIZE"`
}

type SqliteConfig struct {
	Path    string             `yaml:"path" envconfig:"VMDAP_DB_SQLITE_PATH"`
	Version string             `yaml:"version" envconfig:"VMDAP_DB_SQLITE_VERSION"`
	Option  SqliteOptionConfig `yaml:"option"`
}

type Seed struct {
	Version string `yaml:"version" envconfig:"VMDAP_DB_SEED_VERSION"`
}

type BoltSweepConfig struct {
	Interval time.Duration `yaml:"interval" envconfig:"VMDAP_DB_BOLT_SWEEP_INTERVAL"`
}

type BoltConfig struct {
	Path  string          `yaml:"path" envconfig:"VMDAP_DB_BOLT_PATH"`
	Sweep BoltSweepConfig `yaml:"sweep"`
}

type DataBaseConfig struct {
	Bolt    BoltConfig   `yaml:"bolt"`
	SqlLite SqliteConfig `yaml:"sqlite"`
	Seed    Seed         `yaml:"seed"`
}

type LoggerOutputConfig struct {
	File    bool `yaml:"file" envconfig:"VMDAP_LOG_OUTPUT_FILE"`
	Console bool `yaml:"console" envconfig:"VMDAP_LOG_OUTPUT_CONSOLE"`
}

type LoggerConfig struct {
	Path   string             `yaml:"path" envconfig:"VMDAP_LOG_PATH"`
	Pretty bool               `yaml:"pretty" envconfig:"VMDAP_LOG_PRETTY"`
	Output LoggerOutputConfig `yaml:"output" envconfig:"VMDAP_LOG_OUTPUT"`
	Levels map[string]string  `yaml:"levels" envconfig:"VMDAP_LOG_LEVEL"`
}

type Config struct {
	App    AppConfig      `yaml:"app"`
	Listen ListenConfig   `yaml:"listen"`
	DB     DataBaseConfig `yaml:"db"`
	Logger LoggerConfig   `yaml:"logger"`
	path   string
}

func NewConfig(path string) *Config {
	return &Config{
		path: path,
	}
}

func (c *Config) Load() error {
	err := c.loadYaml(c.path)

	if err != nil {
		return err
	}

	cfg := c.loadEnv()

	err = validation.ValidateStruct(
		c,
		c.nested(
			&c.App,
			c.nested(
				&c.App.Web,
				validation.Field(&c.App.Web.Assets, validation.Required),
			),
			c.nested(
				&c.App.Users,
				c.nested(
					&c.App.Users.Root,
					validation.Field(&c.App.Users.Root.Password, validation.Required),
				),
			),
			c.nested(
				&c.App.Security,
				validation.Field(&c.App.Security.Cost, validation.Required),
				c.nested(
					&c.App.Security.Jwt,
					c.nested(
						&c.App.Security.Jwt.Expires,
						validation.Field(&c.App.Security.Jwt.Expires.Access, validation.Required),
						validation.Field(&c.App.Security.Jwt.Expires.Refresh, validation.Required),
					),
					validation.Field(&c.App.Security.Jwt.Secret, validation.Required),
					validation.Field(
						&c.App.Security.Jwt.Algorithm,
						validation.Required,
						validation.In(
							"HS256",
							"HS384",
							"HS512",
						),
					),
				),
			),
		),
		c.nested(
			&c.Listen,
			c.nested(
				&c.Listen.Ports,
				validation.Field(&c.Listen.Ports.Http, validation.Required),
				validation.Field(&c.Listen.Ports.Socket, validation.Required),
			),
			validation.Field(&c.Listen.Host, validation.Required),
			c.nested(
				&c.Listen.Ssl,
				validation.Field(&c.Listen.Ssl.Key, validation.Required),
				validation.Field(&c.Listen.Ssl.Certificate, validation.Required),
			),
		),
		c.nested(
			&c.DB,
			c.nested(
				&c.DB.Seed,
				validation.Field(&c.DB.Seed.Version, validation.Required),
			),
			c.nested(
				&c.DB.SqlLite,
				validation.Field(&c.DB.SqlLite.Version, validation.Required),
				validation.Field(&c.DB.SqlLite.Path, validation.Required),
				c.nested(
					&c.DB.SqlLite.Option,
					validation.Field(&c.DB.SqlLite.Option.JournalMode, validation.Required),
					validation.Field(&c.DB.SqlLite.Option.TempStore, validation.Required),
					validation.Field(&c.DB.SqlLite.Option.Synchronous, validation.Required),
					validation.Field(&c.DB.SqlLite.Option.MmapSize, validation.Required),
				),
			),
		),
		c.nested(
			&c.Logger,
			validation.Field(&c.Logger.Path, validation.Required),
			validation.Field(&c.Logger.Pretty, validation.Required),
			c.nested(
				&c.Logger.Output,
				validation.Field(&c.Logger.Output.File, validation.Required),
				validation.Field(&c.Logger.Output.Console, validation.Required),
			),
			c.nestedMap(
				&c.Logger.Levels,
				validation.Map(
					validation.Key(
						"any",
						validation.Required,
						validation.In(
							"debug",
							"info",
							"warn",
							"error",
							"fatal",
						),
					),
				),
			),
		),
	)

	if err != nil {
		return err
	}

	return cfg
}

func (c *Config) nested(
	target interface{},
	fieldRules ...*validation.FieldRules,
) *validation.FieldRules {
	return validation.Field(
		target,
		validation.By(func(value interface{}) error {
			valueV := reflect.Indirect(reflect.ValueOf(value))

			if valueV.CanAddr() {
				addr := valueV.Addr().Interface()

				return validation.ValidateStruct(addr, fieldRules...)
			}

			return validation.ValidateStruct(target, fieldRules...)
		}),
	)
}

func (c *Config) nestedMap(
	target interface{},
	mapRule validation.MapRule,
) *validation.FieldRules {
	return validation.Field(
		target,
		validation.By(func(value interface{}) error {
			return validation.Validate(target, mapRule)
		}),
	)
}

func (c *Config) loadYaml(path string) error {
	f, err := os.Open(path)

	if err != nil {
		return err
	}

	defer func() {
		err = f.Close()

		if err != nil {
			panic(err)
		}
	}()

	decoder := yaml.NewDecoder(f)

	return decoder.Decode(c)
}

func (c *Config) loadEnv() error {
	return envconfig.Process("", c)
}
