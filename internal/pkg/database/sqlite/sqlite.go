package sqlite

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/go-querystring/query"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/config"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations/seed"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations/sqlite"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"time"
)

type Options struct {
	JournalMode string `url:"journal_mode"`
	Synchronous string `url:"synchronous"`
	TempStore   string `url:"temp_store"`
	MmapSize    int64  `url:"mmap_size"`
}

type migration struct {
	Id        int64
	Version   string
	CreatedAt int64
}

type Sqlite struct {
	client *sql.DB
	logger *logger.Log
	config config.AppConfig
}

func NewSqlite(
	logger *logger.Logger,
	cfg *config.Config,
) (*Sqlite, error) {
	log := logger.GetLogger("db-sqlite")
	log.Info().Msg("connecting to sqlite3 db")

	q, err := query.Values(Options{
		JournalMode: cfg.DB.SqlLite.Option.JournalMode,
		Synchronous: cfg.DB.SqlLite.Option.Synchronous,
		TempStore:   cfg.DB.SqlLite.Option.TempStore,
		MmapSize:    cfg.DB.SqlLite.Option.MmapSize,
	})

	if err != nil {
		return nil, fmt.Errorf(
			"error from encode connection params: %w",
			err,
		)
	}

	db, err := sql.Open(
		"sqlite3",
		fmt.Sprintf(
			"%s?%s",
			cfg.DB.SqlLite.Path,
			q.Encode(),
		),
	)

	if err != nil {
		return nil, fmt.Errorf(
			"error from db connection: %w",
			err,
		)
	}

	err = db.Ping()

	if err != nil {
		return nil, fmt.Errorf("error executing ping to sql lite: %w", err)
	}

	return &Sqlite{
		client: db,
		logger: log,
		config: cfg.App,
	}, nil
}

func (s Sqlite) GetClient() *sql.DB {
	return s.client
}

func (s Sqlite) Close() error {
	return s.client.Close()
}

func (s Sqlite) Migrate(ctx context.Context, ver string) error {
	s.logger.Info().Msg("Starting migration")
	err := s.init(ctx)

	if err != nil {
		return err
	}

	version, err := s.getVersion(ctx, migrations.MigrationTypeMigrate)

	if err != nil {
		return err
	}

	for _, m := range s.getMigrations(version, ver, migrations.MigrationTypeMigrate) {
		tx, txErr := s.client.BeginTx(ctx, &sql.TxOptions{})

		if txErr != nil {
			return fmt.Errorf("error create new transaction: %w", err)
		}

		var vErr error

		if m.Dir == migrations.Up {
			vErr = s.setVersion(ctx, tx, m.Version, migrations.MigrationTypeMigrate)
		} else if m.Dir == migrations.Down {
			vErr = s.deleteVersion(ctx, tx, m.Version, migrations.MigrationTypeMigrate)
		}

		if vErr != nil {
			_ = tx.Rollback()

			return fmt.Errorf("error set migration version: %w", err)
		}

		mErr := m.Fn(migrations.MigrationParams{
			Ctx:    ctx,
			Client: tx,
			Cfg:    s.config,
		})

		if mErr != nil {
			_ = tx.Rollback()

			return fmt.Errorf("error from run migration: %w", mErr)
		}

		err = tx.Commit()

		if err != nil {
			return fmt.Errorf("error commit migration transaction: %w", err)
		}
	}

	s.logger.Info().Msg("Finish migration")

	return nil
}

func (s Sqlite) Seed(ctx context.Context, ver string) error {
	s.logger.Info().Msg("Starting seed")
	version, err := s.getVersion(ctx, migrations.MigrationTypeSeed)

	if err != nil {
		return err
	}

	for _, m := range s.getMigrations(version, ver, migrations.MigrationTypeSeed) {
		tx, txErr := s.client.BeginTx(ctx, &sql.TxOptions{})

		if txErr != nil {
			return fmt.Errorf("error create new transaction: %w", err)
		}

		var vErr error

		if m.Dir == migrations.Up {
			vErr = s.setVersion(ctx, tx, m.Version, migrations.MigrationTypeSeed)
		} else if m.Dir == migrations.Down {
			vErr = s.deleteVersion(ctx, tx, m.Version, migrations.MigrationTypeSeed)
		}

		if vErr != nil {
			_ = tx.Rollback()

			return fmt.Errorf("error set migration version: %w", err)
		}

		mErr := m.Fn(migrations.MigrationParams{
			Ctx:    ctx,
			Client: tx,
			Cfg:    s.config,
		})

		if mErr != nil {
			_ = tx.Rollback()

			return fmt.Errorf("error from run migration: %w", mErr)
		}

		err = tx.Commit()

		if err != nil {
			return fmt.Errorf("error commit migration transaction: %w", err)
		}
	}

	s.logger.Info().Msg("Finish seed")

	return nil
}

func (s Sqlite) init(ctx context.Context) error {
	_, err := s.client.ExecContext(
		ctx,
		`
			CREATE TABLE IF NOT EXISTS t_migrations (
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				version TEXT NOT NULL,
				created_at INTEGER NOT NULL
			);

			CREATE TABLE IF NOT EXISTS t_seeds (
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				version TEXT NOT NULL,
				created_at INTEGER NOT NULL
			);
		`,
	)

	if err != nil {
		return fmt.Errorf("error creating migration table: %w", err)
	}

	return nil
}

func (s Sqlite) getVersion(
	ctx context.Context,
	migrationType migrations.MigrationType,
) (string, error) {
	table := "t_migrations"

	if migrationType == migrations.MigrationTypeSeed {
		table = "t_seeds"
	}

	result := s.client.QueryRowContext(
		ctx,
		fmt.Sprintf(
			`
				SELECT * 
				FROM %s 
				ORDER BY version DESC
				LIMIT 1;
			`,
			table,
		),
	)

	if result.Err() != nil {
		return "", fmt.Errorf(
			"error getting last migration version: %w",
			result.Err(),
		)
	}

	m := migration{}
	err := result.Scan(&m.Id, &m.Version, &m.CreatedAt)

	if err != nil {
		if err == sql.ErrNoRows {
			return "", nil
		}

		return "", fmt.Errorf(
			"error scan migration row: %w",
			err,
		)
	}

	return m.Version, nil
}

func (s Sqlite) getMigrations(
	version string,
	ver string,
	migrationType migrations.MigrationType,
) []migrations.Migration {
	if version == ver {
		return nil
	}

	d := migrations.Up

	if version > ver {
		d = migrations.Down
	}

	mMap := sqlite.GetRegistry()

	if migrationType == migrations.MigrationTypeSeed {
		mMap = seed.GetRegistry()
	}

	mList := make([]migrations.Migration, 0, len(mMap))

	for mVersion, mFns := range mMap {
		switch true {
		case d == migrations.Up && mVersion > version && mVersion <= ver:
			mList = append(
				mList,
				migrations.Migration{
					Fn:      mFns[d],
					Version: mVersion,
					Dir:     d,
				},
			)
		case d == migrations.Down && mVersion <= version && mVersion > ver:
			mList = append(
				mList,
				migrations.Migration{
					Fn:      mFns[d],
					Version: mVersion,
					Dir:     d,
				},
			)
		}
	}

	return mList
}

func (s Sqlite) setVersion(
	ctx context.Context,
	tx *sql.Tx,
	version string,
	migrationType migrations.MigrationType,
) error {
	table := "t_migrations"

	if migrationType == migrations.MigrationTypeSeed {
		table = "t_seeds"
	}

	stmt, err := tx.PrepareContext(
		ctx,
		fmt.Sprintf(
			`
				INSERT INTO %s(version, created_at)
				VALUES(?, ?);
			`,
			table,
		),
	)

	if err != nil {
		return fmt.Errorf("error prepare set version query: %w", err)
	}

	_, err = stmt.ExecContext(
		ctx,
		version,
		time.Now().Unix(),
	)

	if err != nil {
		return fmt.Errorf("error exec set version query: %w", err)
	}

	return nil
}

func (s Sqlite) deleteVersion(
	ctx context.Context,
	tx *sql.Tx,
	version string,
	migrationType migrations.MigrationType,
) error {
	table := "t_migrations"

	if migrationType == migrations.MigrationTypeSeed {
		table = "t_seeds"
	}

	stmt, err := tx.PrepareContext(
		ctx,
		fmt.Sprintf(
			`
				DELETE FROM %s
				WHERE version = ?;
			`,
			table,
		),
	)

	if err != nil {
		return fmt.Errorf("error prepare delete version query: %w", err)
	}

	_, err = stmt.ExecContext(
		ctx,
		version,
	)

	if err != nil {
		return fmt.Errorf("error exec delete version query: %w", err)
	}

	return nil
}
