package seed

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
	"time"
)

func init() {
	version := "1633210358fd216850createrootrole"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Up] = Up1633210358fd216850createrootrole
}

func Up1633210358fd216850createrootrole(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	stmt, err := client.PrepareContext(
		params.Ctx,
		`
			INSERT INTO t_roles(name, description, created_at, updated_at)
			VALUES(?, ?, ?, ?);
		`,
	)

	if err != nil {
		return fmt.Errorf("error prepare set version query: %w", err)
	}

	_, err = stmt.ExecContext(
		params.Ctx,
		"root",
		"Role that is allowed to do everything",
		time.Now().Unix(),
		time.Now().Unix(),
	)

	return err
}
