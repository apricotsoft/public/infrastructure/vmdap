package seed

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "1633210387a9d10d54assigenrootroletouser"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Down] = Down1633210387a9d10d54assigenrootroletouser
}

func Down1633210387a9d10d54assigenrootroletouser(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	stmt, err := client.PrepareContext(
		params.Ctx,
		`
			DELETE FROM t_user_roles
			WHERE
    			user_id = (SELECT id FROM t_users WHERE login = ?)
    			AND role_id = (SELECT id FROM t_roles WHERE name = ?);
		`,
	)

	if err != nil {
		return fmt.Errorf("error prepare delete version query: %w", err)
	}

	_, err = stmt.ExecContext(
		params.Ctx,
		"root",
		"root",
	)

	return err
}
