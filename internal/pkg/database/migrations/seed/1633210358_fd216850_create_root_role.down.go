package seed

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "1633210358fd216850createrootrole"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Down] = Down1633210358fd216850createrootrole
}

func Down1633210358fd216850createrootrole(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	stmt, err := client.PrepareContext(
		params.Ctx,
		`
			DELETE FROM t_roles
			WHERE name = ?;
		`,
	)

	if err != nil {
		return fmt.Errorf("error prepare delete version query: %w", err)
	}

	_, err = stmt.ExecContext(
		params.Ctx,
		"root",
	)

	return err
}
