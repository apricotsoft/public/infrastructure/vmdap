package seed

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
	"golang.org/x/crypto/bcrypt"
	"time"
)

func init() {
	version := "16322386027d31a009createrootuser"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Up] = Up16322386027d31a009createrootuser
}

func Up16322386027d31a009createrootuser(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	stmt, err := client.PrepareContext(
		params.Ctx,
		`
			INSERT INTO t_users(login, password, created_at, updated_at)
			VALUES(?, ?, ?, ?);
		`,
	)

	if err != nil {
		return fmt.Errorf("error prepare set version query: %w", err)
	}

	pass, err := bcrypt.GenerateFromPassword(
		[]byte(params.Cfg.Users.Root.Password),
		params.Cfg.Security.Cost,
	)

	if err != nil {
		return fmt.Errorf("error encript root user password: %w", err)
	}

	_, err = stmt.ExecContext(
		params.Ctx,
		"root",
		string(pass),
		time.Now().UnixNano(),
		time.Now().UnixNano(),
	)

	return err
}
