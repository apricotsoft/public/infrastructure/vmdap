package seed

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "16322386027d31a009createrootuser"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Down] = Down16322386027d31a009createrootuser
}

func Down16322386027d31a009createrootuser(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	stmt, err := client.PrepareContext(
		params.Ctx,
		`
			DELETE FROM t_users
			WHERE login = ?;
		`,
	)

	if err != nil {
		return fmt.Errorf("error prepare delete version query: %w", err)
	}

	_, err = stmt.ExecContext(
		params.Ctx,
		"root",
	)

	return err
}
