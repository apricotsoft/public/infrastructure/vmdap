package seed

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
	"time"
)

func init() {
	version := "1633210387a9d10d54assigenrootroletouser"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Up] = Up1633210387a9d10d54assigenrootroletouser
}

func Up1633210387a9d10d54assigenrootroletouser(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	stmt, err := client.PrepareContext(
		params.Ctx,
		`
			INSERT INTO t_user_roles (
    			user_id,
    			role_id,
    			created_at
			)
			VALUES (
				(SELECT id FROM t_users WHERE login = ?),
				(SELECT id FROM t_roles WHERE name = ?),
				?
			);
		`,
	)

	if err != nil {
		return fmt.Errorf("error prepare set version query: %w", err)
	}

	_, err = stmt.ExecContext(
		params.Ctx,
		"root",
		"root",
		time.Now().UTC().UnixNano(),
	)

	return err
}
