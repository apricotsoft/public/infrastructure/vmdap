package sqlite

import (
	"database/sql"
	"errors"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "1632349679987098d0createsessiontable"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Up] = Up1632349679987098d0createsessiontable
}

func Up1632349679987098d0createsessiontable(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	_, err := client.ExecContext(
		params.Ctx,
		`
			CREATE TABLE IF NOT EXISTS t_sessions (
				id TEXT PRIMARY KEY,
				user_id INTEGER NOT NULL,
				ip TEXT NOT NULL,
				created_at INTEGER NOT NULL,
				expired_at INTEGER NOT NULL,
				FOREIGN KEY(user_id) REFERENCES t_users(id)
			);
		`,
	)

	return err
}
