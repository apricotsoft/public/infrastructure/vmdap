package sqlite

import (
	"database/sql"
	"errors"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "1633412148c9d40294createactionstable"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Down] = Down1633412148c9d40294createactionstable
}

func Down1633412148c9d40294createactionstable(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	_, err := client.ExecContext(
		params.Ctx,
		"DROP TABLE IF EXISTS t_actions;",
	)

	return err
}
