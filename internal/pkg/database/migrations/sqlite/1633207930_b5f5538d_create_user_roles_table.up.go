package sqlite

import (
	"database/sql"
	"errors"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "1633207930b5f5538dcreateuserrolestable"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Up] = Up1633207930b5f5538dcreateuserrolestable
}

func Up1633207930b5f5538dcreateuserrolestable(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	_, err := client.ExecContext(
		params.Ctx,
		`
			CREATE TABLE IF NOT EXISTS t_user_roles (
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				user_id INTEGER NOT NULL,
				role_id INTEGER NOT NULL,
				created_at INTEGER NOT NULL,
				deleted_at INTEGER NULL,
				FOREIGN KEY(user_id) REFERENCES t_users(id),
				FOREIGN KEY(role_id) REFERENCES t_roles(id)
			);
		`,
	)

	return err
}
