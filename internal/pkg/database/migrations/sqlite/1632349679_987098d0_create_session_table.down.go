package sqlite

import (
	"database/sql"
	"errors"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "1632349679987098d0createsessiontable"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Down] = Down1632349679987098d0createsessiontable
}

func Down1632349679987098d0createsessiontable(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	_, err := client.ExecContext(
		params.Ctx,
		"DROP TABLE IF EXISTS t_sessions;",
	)

	return err
}
