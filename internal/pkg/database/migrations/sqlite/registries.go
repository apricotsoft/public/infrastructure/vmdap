package sqlite

import "gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"

var registry = make(migrations.Registry)

func GetRegistry() migrations.Registry {
	return registry
}
