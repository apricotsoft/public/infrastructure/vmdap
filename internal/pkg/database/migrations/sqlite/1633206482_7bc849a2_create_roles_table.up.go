package sqlite

import (
	"database/sql"
	"errors"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "16332064827bc849a2createrolestable"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Up] = Up16332064827bc849a2createrolestable
}

func Up16332064827bc849a2createrolestable(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	_, err := client.ExecContext(
		params.Ctx,
		`
			CREATE TABLE IF NOT EXISTS t_roles (
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				name TEXT NOT NULL,
				description TEXT NOT NULL,
				created_at INTEGER NOT NULL,
				updated_at INTEGER NOT NULL,
				deleted_at INTEGER NULL
			);
		`,
	)

	return err
}
