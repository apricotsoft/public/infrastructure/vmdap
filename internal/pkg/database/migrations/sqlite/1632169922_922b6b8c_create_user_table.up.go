package sqlite

import (
	"database/sql"
	"errors"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "1632169922922b6b8ccreateusertable"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Up] = Up1632169922922b6b8ccreateusertable
}

func Up1632169922922b6b8ccreateusertable(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	_, err := client.ExecContext(
		params.Ctx,
		`
			CREATE TABLE IF NOT EXISTS t_users (
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				login TEXT NOT NULL,
				password TEXT NOT NULL,
				created_at INTEGER NOT NULL,
				updated_at INTEGER NOT NULL
			);
			CREATE UNIQUE INDEX idx_users_login 
			ON t_users (login);
		`,
	)

	return err
}
