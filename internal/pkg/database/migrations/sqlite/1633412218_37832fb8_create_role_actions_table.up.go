package sqlite

import (
	"database/sql"
	"errors"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
)

func init() {
	version := "163341221837832fb8createroleactionstable"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][migrations.Up] = Up163341221837832fb8createroleactionstable
}

func Up163341221837832fb8createroleactionstable(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)

	if !ok {
		return errors.New("invalid connection parameters")
	}

	_, err := client.ExecContext(
		params.Ctx,
		`
			CREATE TABLE IF NOT EXISTS t_role_actions (
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				role_id INTEGER NOT NULL,
				action_id INTEGER NOT NULL,
				created_at INTEGER NOT NULL,
				deleted_at INTEGER NULL,
				FOREIGN KEY(role_id) REFERENCES t_roles(id),
				FOREIGN KEY(action_id) REFERENCES t_actions(id)
			);
		`,
	)

	return err
}
