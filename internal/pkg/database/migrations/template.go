package migrations

var TypeTemplateMap = map[DBType]func() string{
	DBTypeSqlite: sqlite,
	DBTypeSeed:   seed,
}

func seed() string {
	return `package seed

import (
	"database/sql"
	"errors"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
	"golang.org/x/net/context"
)

func init() {
	version := "{{.Version}}"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][{{.Direction}}] = {{.FunctionName}}
}

func {{.FunctionName}}(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)
	
	if !ok {
		return errors.New("invalid connection parameters")
	}
	
	client.QueryRowContext(context.Background(), "")
	
	return errors.New("not implemented")
}`
}

func sqlite() string {
	return `package sqlite

import (
	"database/sql"
	"errors"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/database/migrations"
	"golang.org/x/net/context"
)

func init() {
	version := "{{.Version}}"

	_, ok := registry[version]

	if !ok {
		registry[version] = make(map[migrations.Direction]migrations.MigrationFn)
	}

	registry[version][{{.Direction}}] = {{.FunctionName}}
}

func {{.FunctionName}}(params migrations.MigrationParams) error {
	client, ok := params.Client.(*sql.Tx)
	
	if !ok {
		return errors.New("invalid connection parameters")
	}
	
	client.QueryRowContext(context.Background(), "")
	
	return errors.New("not implemented")
}`
}
