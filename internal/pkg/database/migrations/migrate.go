package migrations

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/config"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync/atomic"
	"text/template"
	"time"
)

var counter = readRandomUint32()
var log = logger.NewLogger(config.LoggerConfig{
	Pretty: true,
	Output: config.LoggerOutputConfig{
		File:    false,
		Console: true,
	},
	Levels: map[string]string{"any": "info"},
}).GetLogger()

type Direction string

const (
	Down Direction = "down"
	Up   Direction = "up"
)

type DBType int32

const (
	DBTypeNone   DBType = 0
	DBTypeSqlite DBType = 1
	DBTypeSeed   DBType = 2
)

type MigrationType int32

const (
	MigrationTypeNone    MigrationType = 0
	MigrationTypeMigrate MigrationType = 1
	MigrationTypeSeed    MigrationType = 2
)

var TypeValueMap = map[string]DBType{
	"sqlite": DBTypeSqlite,
	"seed":   DBTypeSeed,
}

var ValueTypeMap = map[DBType]string{
	DBTypeSqlite: "sqlite",
	DBTypeSeed:   "seed",
}

type MigrationParams struct {
	Ctx    context.Context
	Client interface{}
	Cfg    config.AppConfig
}

type MigrationFn func(params MigrationParams) error

type Migration struct {
	Fn      MigrationFn
	Version string
	Dir     Direction
}

type Registry map[string]map[Direction]MigrationFn

type Generator struct {
	Type DBType
	Name string
}

type fileOptions struct {
	Name      string
	FullNames map[string]Direction
}

type templateData struct {
	Version       string
	FunctionName  string
	Direction     string
	DirectionType Direction
}

func NewGenerator(dbType, name string) (*Generator, error) {
	t, ok := TypeValueMap[dbType]

	if !ok {
		return nil, errors.New("db type not found")
	}

	return &Generator{
		Type: t,
		Name: name,
	}, nil
}

func (g *Generator) Generate() error {
	info, err := g.fileName()

	if err != nil {
		return err
	}

	path, err := g.path()

	if err != nil {
		return err
	}

	for name, direction := range info.FullNames {
		file, fileErr := os.Create(filepath.Join(path, name))

		if fileErr != nil {
			return fileErr
		}

		migrationName := strings.ReplaceAll(info.Name, "_", "")
		var buffer bytes.Buffer
		d := &templateData{
			Version: migrationName,
			FunctionName: fmt.Sprintf(
				"%s%s",
				strings.Title(string(direction)),
				migrationName,
			),
			Direction:     getTemplateDirection(direction),
			DirectionType: direction,
		}

		tpl, ok := TypeTemplateMap[g.Type]

		if ok {
			t := template.Must(template.New("file").Parse(tpl()))
			templateErr := t.Execute(&buffer, d)

			if templateErr != nil {
				return templateErr
			}
		}

		_, err = file.Write(buffer.Bytes())

		if err != nil {
			return err
		}
	}

	return nil
}

func (g *Generator) fileName() (*fileOptions, error) {
	now := time.Now().Unix()
	prefix := strconv.FormatInt(int64(atomic.AddUint32(&counter, 1)), 16)
	name := fmt.Sprintf("%d_%s_%s", now, prefix, g.Name)

	return &fileOptions{
		Name: name,
		FullNames: map[string]Direction{
			fmt.Sprintf("%s.up.go", name):   Up,
			fmt.Sprintf("%s.down.go", name): Down,
		},
	}, nil
}

func (g *Generator) path() (string, error) {
	return filepath.Abs(
		filepath.Join(
			"..",
			"..",
			"internal",
			"pkg",
			"database",
			"migrations",
			ValueTypeMap[g.Type],
		),
	)
}

func readRandomUint32() uint32 {
	return uint32(time.Now().UnixNano())
}

func getTemplateDirection(direction Direction) string {
	switch direction {
	case Up:
		return "migrations.Up"
	case Down:
		return "migrations.Down"
	default:
		return ""
	}
}
