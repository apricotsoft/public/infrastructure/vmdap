package bolt

import (
	"context"
	"encoding/binary"
	"fmt"
	"github.com/boltdb/bolt"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/config"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/logger"
	"time"
)

type migration struct {
	Name []byte
	TTL  bool
}

var buckets = []migration{
	{
		Name: []byte("activities"),
		TTL:  false,
	},
	{
		Name: []byte("tokens"),
		TTL:  true,
	},
}

type Bolt struct {
	client *bolt.DB
	logger *logger.Log
	config config.BoltConfig
}

func NewBolt(
	logger *logger.Logger,
	cfg config.BoltConfig,
) (*Bolt, error) {
	log := logger.GetLogger("db-bolt")
	log.Info().Msg("connecting to bolt db")

	db, err := bolt.Open(
		cfg.Path,
		0600,
		&bolt.Options{
			Timeout: 1 * time.Second,
		},
	)

	if err != nil {
		return nil, fmt.Errorf(
			"error from db connection: %w",
			err,
		)
	}

	instance := &Bolt{
		client: db,
		logger: log,
		config: cfg,
	}

	err = instance.migrate()

	if err != nil {
		return nil, fmt.Errorf(
			"error from db migration: %w",
			err,
		)
	}

	return instance, nil
}

func (b Bolt) GetClient() *bolt.DB {
	return b.client
}

func (b Bolt) Close() error {
	return b.client.Close()
}

func (b Bolt) Sweep(ctx context.Context) error {
	for {
		select {
		case <-ctx.Done():
			return nil
		default:
		}

		b.sweep()
		time.Sleep(b.config.Sweep.Interval)
	}
}

func (b Bolt) sweep() {
	b.logger.Info().Msg("Run key ttl handler")
	keys, err := b.getExpired(uint64(time.Now().UTC().UnixNano()))

	if err != nil {
		b.logger.Error().Err(err).Send()

		return
	}

	if len(keys) == 0 {
		return
	}

	err = b.client.Update(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte("tokens"))
		bSecret := root.Bucket([]byte("values"))

		for _, key := range keys {
			_ = bSecret.Delete(key)
		}

		return nil
	})

	if err != nil {
		b.logger.Error().Err(err).Send()

		return
	}
}

func (b Bolt) getExpired(maxAge uint64) ([][]byte, error) {
	keys := make([][]byte, 0, 1000)
	ttlKeys := make([][]byte, 0, 1000)

	err := b.client.View(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte("tokens"))
		c := root.Bucket([]byte("ttl")).Cursor()

		for k, v := c.First(); k != nil && binary.LittleEndian.Uint64(k) < maxAge; k, v = c.Next() {
			keys = append(keys, v)
			ttlKeys = append(ttlKeys, k)
		}

		return nil
	})

	if err != nil {
		return nil, fmt.Errorf(
			"error from collect deleted keys: %w",
			err,
		)
	}

	err = b.client.Update(func(tx *bolt.Tx) error {
		root := tx.Bucket([]byte("tokens"))
		ttl := root.Bucket([]byte("ttl"))

		for _, key := range ttlKeys {
			if err = ttl.Delete(key); err != nil {
				return err
			}
		}

		return nil
	})

	if err != nil {
		return nil, fmt.Errorf(
			"error from collect deleted keys: %w",
			err,
		)
	}

	return nil, nil
}

func (b Bolt) migrate() error {
	tx, err := b.client.Begin(true)

	if err != nil {
		return fmt.Errorf(
			"error from open transaction: %w",
			err,
		)
	}

	fail := false

	for _, bucket := range buckets {
		root, rootErr := tx.CreateBucketIfNotExists(bucket.Name)

		if rootErr != nil {
			b.logger.
				Error().
				Err(rootErr).
				Msgf("error from create bucket '%s'", bucket.Name)
			fail = true

			break
		}

		if bucket.TTL {
			_, err = root.CreateBucketIfNotExists([]byte("ttl"))

			if err != nil {
				b.logger.Error().Err(err).Msg("error from create nested ttl bucket")
				fail = true

				break
			}

			_, err = root.CreateBucketIfNotExists([]byte("values"))

			if err != nil {
				b.logger.Error().Err(err).Msg("error from create nested values bucket")
				fail = true

				break
			}
		}
	}

	if fail {
		err = tx.Rollback()
	} else {
		err = tx.Commit()
	}

	if err != nil {
		return fmt.Errorf(
			"error from close transaction: %w",
			err,
		)
	}

	return nil
}
