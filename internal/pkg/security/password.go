package security

import "golang.org/x/crypto/bcrypt"

type Password struct {
	cost int
}

func NewPassword(cost int) *Password {
	return &Password{
		cost: cost,
	}
}

func (p Password) encrypt(pass string) ([]byte, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(pass), p.cost)

	if err != nil {
		return nil, err
	}

	return hash, nil
}

func (p Password) Check(pass, hash string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(pass))

	if err != nil {
		return false, err
	}

	return true, nil
}
