package security

import (
	"errors"
	"github.com/gbrlsnchs/jwt/v3"
	"gitlab.com/apricotsoft/public/infrastructure/vmdap/internal/pkg/config"
	"time"
)

type TokenType string

const (
	TypeAccess  TokenType = "access"
	TypeRefresh TokenType = "refresh"
)

type Payload struct {
	jwt.Payload
	Id   uint64    `json:"id"`
	Type TokenType `json:"type"`
}

type jwtToken struct {
	Access  string
	Refresh string
	Expired int64
}

type Jwt struct {
	config config.AppSecurityJwtConfig
	hs     *jwt.HMACSHA
}

func NewJwt(cfg config.AppSecurityJwtConfig) *Jwt {
	secret := []byte(cfg.Secret)
	hs := &jwt.HMACSHA{}

	switch cfg.Algorithm {
	case "HS256":
		hs = jwt.NewHS256(secret)
	case "HS384":
		hs = jwt.NewHS384(secret)
	case "HS512":
		hs = jwt.NewHS512(secret)
	}

	return &Jwt{
		config: cfg,
		hs:     hs,
	}
}

func (j Jwt) Encode(userId uint64, sessionId string) (*jwtToken, error) {
	now := time.Now().UTC()
	tokenPayload := Payload{
		Payload: jwt.Payload{
			ExpirationTime: jwt.NumericDate(now.Add(j.config.Expires.Access)),
			IssuedAt:       jwt.NumericDate(now),
			JWTID:          sessionId,
		},
		Id:   userId,
		Type: TypeAccess,
	}
	refreshPayload := Payload{
		Payload: jwt.Payload{
			ExpirationTime: jwt.NumericDate(now.Add(j.config.Expires.Refresh)),
			IssuedAt:       jwt.NumericDate(now),
			JWTID:          sessionId,
		},
		Id:   userId,
		Type: TypeRefresh,
	}

	token, err := jwt.Sign(tokenPayload, j.hs)

	if err != nil {
		return nil, err
	}

	refreshToken, err := jwt.Sign(refreshPayload, j.hs)

	if err != nil {
		return nil, err
	}

	result := &jwtToken{
		Access:  string(token),
		Refresh: string(refreshToken),
		Expired: tokenPayload.ExpirationTime.UnixNano(),
	}

	return result, nil
}

func (j Jwt) Decode(token string) (Payload, error) {
	var decodedToken Payload

	if _, err := jwt.Verify([]byte(token), j.hs, &decodedToken); err != nil {
		return Payload{}, errors.New("invalid payload")
	}

	if decodedToken.Id == 0 {
		return Payload{}, errors.New("missing id in payload")
	}

	if len(decodedToken.JWTID) == 0 {
		return Payload{}, errors.New("missing session id in payload")
	}

	if decodedToken.ExpirationTime.Time.Unix() < time.Now().UTC().Unix() {
		if decodedToken.Type == TypeAccess {
			return Payload{}, errors.New("access token expired")
		} else if decodedToken.Type == TypeRefresh {
			return Payload{}, errors.New("refresh token expired")
		}
	}

	return decodedToken, nil
}
