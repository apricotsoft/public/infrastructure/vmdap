package domains

import "time"

type RoleAction struct {
	Id        uint64
	RoleId    uint64
	ActionId  uint64
	CreatedAt time.Time
	DeletedAt time.Time
}
