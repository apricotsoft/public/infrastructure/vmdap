package domains

import "time"

type User struct {
	Id        uint64
	Login     string
	Password  string
	CreatedAt time.Time
	UpdatedAt time.Time
}
