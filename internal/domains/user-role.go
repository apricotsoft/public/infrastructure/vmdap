package domains

import "time"

type UserRole struct {
	Id        uint64
	UserId    uint64
	RoleId    uint64
	CreatedAt time.Time
	DeletedAt time.Time
}
