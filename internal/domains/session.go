package domains

import "time"

type Session struct {
	Id        string
	UserId    uint64
	Ip        string
	CreatedAt time.Time
	ExpiredAt time.Time
}
