package domains

import "time"

type ActivityMethod string

const (
	ActivityMethodGet    ActivityMethod = "get"
	ActivityMethodPost   ActivityMethod = "post"
	ActivityMethodPut    ActivityMethod = "put"
	ActivityMethodDelete ActivityMethod = "delete"
)

type Activity struct {
	UserId    uint64
	SessionId string
	Date      time.Time
	Endpoint  string
	Method    ActivityMethod
	ActionId  *int64
}
